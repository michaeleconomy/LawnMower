﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DialogVariables : MonoBehaviour {
    public static DialogVariables instance;

    public HashSet<string> variables = new HashSet<string>();

    private void Awake() {
        instance = this;
    }

    public void Clear() {
        variables.Clear();
    }

    public void Set(string variableName) {
        variables.Add(variableName);
    }

    public bool Get(string variableName) {
        return variables.Contains(variableName);
    }

    public void Remove(string variableName) {
        variables.Remove(variableName);
    }
}
