﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogCommands : MonoBehaviour {
    private ManagedPrefab speaker;

    public IEnumerator RunCommand(string command) {
        speaker = DialogRunner.instance.speaker;
        var commandPieces = command.Split(' ');
        var arguments = commandPieces.GetRange(1);
        switch (commandPieces[0]) {
            case "meet":
                DialogVariables.instance.Set("met" + speaker.name);
                yield break;
            case "startQuest":
                if (arguments.Empty()) {
                    Debug.LogError("missing the quest id parameter from command " + command);
                    yield break;
                }
                QuestManager.instance.BeginQuest(arguments[0]);
                yield break;
            case "completeObjective":
                if (arguments.Empty()) {
                    Debug.LogError("missing the quest id parameter from command " + command);
                    yield break;
                }
                QuestManager.instance.CompleteObjective(arguments[0]);
                yield break;
            case "abandonQuest":
                if (arguments.Empty()) {
                    Debug.LogError("missing the quest id parameter from command " + command);
                    yield break;
                }
                QuestManager.instance.AbandonIfActive(arguments[0]);
                yield break;
            case "giveItem":
                if (arguments.Empty()) {
                    Debug.LogError("missing the item name parameter from command " + command);
                    yield break;
                }
                yield return StartCoroutine(GiveItem(arguments[0]));
                yield break;
            case "hide":
                if (arguments.Empty()) {
                    Debug.LogError("missing the name parameter from command " + command);
                    yield break;
                }
                Hide(arguments);
                yield break;
            case "unhide":
                if (arguments.Empty()) {
                    Debug.LogError("missing the name parameter from command " + command);
                    yield break;
                }
                Unhide(arguments);
                yield break;
            case "set":
                if (arguments.Empty()) {
                    Debug.LogError("missing variable name: " + command);
                    yield break;
                }
                DialogVariables.instance.Set(arguments[0]);
                yield break;
            case "leave":
                Leave();
                yield break;
            case "attack":
                Attack();
                yield break;
            case "join":
                yield return StartCoroutine(Join());
                yield break;
            case "trigger":
                if (arguments.Length < 2) {
                    Debug.LogError("trigger requires two parameters (trigger triggerName timeOffset) " + command);
                    yield break;
                }
                int timeOffset;
                if (!int.TryParse(arguments[1], out timeOffset)) {
                    Debug.LogError("trigger triggerName timeOffset(int turns)");
                    yield break;
                }
                TriggerManager.instance.ReadyTrigger(arguments[0], timeOffset);
                yield break;
            case "travel":
                if (arguments.Length < 3) {
                    Debug.LogError("missing parameters for travel " + command +
                                   " - format is travel AREA DESTINATION_NAME TRAVELTIME(int - minutes)");
                    yield break;
                }
                int time;
                if (!int.TryParse(arguments[2], out time)) {
                    Debug.LogError("travel AREA DESTINATION_NAME TRAVELTIME(int - minutes)");
                    yield break;
                }
                PlayerMapManager.instance.Travel(arguments[0], arguments[1], time);
                yield break;
            default:
                Debug.LogError("invalid command: " + command);
                yield break;
        }
    }

    private void Attack() {
        if (!(speaker is NonPlayerCharacter)) {
            Debug.LogError(speaker.name + " can't attack, not an NPC");
            return;
        }
        var npc = (NonPlayerCharacter)speaker;
        npc.friendly = false;
    }

    private void Leave() {
        throw new NotImplementedException();
        //TODO move

        //TODO self destruct
    }

    private IEnumerator Join() {
        if (!(speaker is NonPlayerCharacter)) {
            Debug.LogError(speaker.name + " can't join, not an NPC");
            yield break;
        }
        var npc = (NonPlayerCharacter)speaker;
        PlayerManager.instance.ConvertToPc(npc);
        var line = npc.name + " has joined your party.";
        yield return StartCoroutine(DialogRunner.instance.view.ShowDialog(line));
    }

    private void Unhide(params string[] names) {
        foreach (var n in names) {
            var entity = World.instance.FindChildByName<ManagedPrefab>(n);
            if (entity == null) {
                Debug.LogWarning("entity could not be found: " + n);
                continue;
            }
            entity.SetActive(true);
        }
    }

    private void Hide(params string[] names) {
        foreach (var n in names) {
            var entity = World.instance.FindChildByName<ManagedPrefab>(n);
            if (entity == null) {
                Debug.LogWarning("entity could not be found: " + n);
                continue;
            }
            entity.SetActive(false);
        }
    }

    private IEnumerator GiveItem(string itemName) {
        if (!(speaker is GameEntity)) {
            Debug.LogWarning(speaker.name + " is not a gameEntity, and cannot contain things");
            yield break;
        }
        var gameEntity = (GameEntity)speaker;
        if (!gameEntity.isContainer) {
            Debug.LogWarning(speaker.name + " is not a container");
            yield break;
        }
        var item = gameEntity.GetItemByName(itemName);
        if (item == null) {
            Debug.LogWarning(gameEntity.name + " doesn't have a " + itemName);
            yield break;
        }
        gameEntity.Remove(item);
        PlayerCharacter.primaryPlayer.Add(item);
        var line = gameEntity.name + " gives you a " + itemName;
        yield return StartCoroutine(DialogRunner.instance.view.ShowDialog(line));
    }
}
