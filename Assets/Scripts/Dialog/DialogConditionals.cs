﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class DialogConditionals : MonoBehaviour {
    Regex andRegex = new Regex(@"^(?<left>(?:[^\(\)]|(?<paren>\()|(?<-paren>\)))+)(?(paren)(?!))&&(?<right>.+)$");
    Regex orRegex = new Regex(@"^^(?<left>(?:[^\(\)]|(?<paren>\()|(?<-paren>\)))+)(?(paren)(?!))\|\|(?<right>.+)$");
    Regex methodCallRegex = new Regex(@"^(?<method>[\w]+)\(((?<param>[^,()]+)(?:(?:\,?)(?<param>[^,()]+))*)?\)$");
    Regex variableRegex = new Regex(@"^\w+$");

    public bool Evaluate(string conditional) {
        conditional = conditional.Trim();
        if (WrappedInParens(conditional)) {
            return Evaluate(conditional.Substring(1, conditional.Length - 2));
        }

        var andMatch = andRegex.Match(conditional);
        if (andMatch.Success) {
            return Evaluate(andMatch.Groups["left"].Value) && Evaluate(andMatch.Groups["right"].Value);
        }

        var orMatch = orRegex.Match(conditional);
        if (orMatch.Success) {
            return Evaluate(orMatch.Groups["left"].Value) || Evaluate(orMatch.Groups["right"].Value);
        }

        if (conditional.StartsWith("!")) {
            return !Evaluate(conditional.Substring(1));
        }

        var variableMatch = variableRegex.Match(conditional);
        if (variableMatch.Success) {
            return DialogVariables.instance.Get(conditional);
        }

        var methodMatch = methodCallRegex.Match(conditional);
        if (methodMatch.Success) {
            var parameters = new List<string>();
            var captures = methodMatch.Groups["param"].Captures;
            for (var i = 0; i < captures.Count; i++) {
                var capture = captures[i];
                parameters.Add(capture.Value.Trim());
            }
            return EvaluateMethod(methodMatch.Groups["method"].Value, parameters);
        }

        throw new Exception("unrecognized format: " + conditional);
    }

    private bool WrappedInParens(string conditional) {
        if (!conditional.StartsWith("(") || !conditional.EndsWith(")")) {
            return false;
        }

        var parenLevel = 0;
        for (var i = 1; i < conditional.Length - 1; i++) {
            switch (conditional[i]) {
                case '(':
                    parenLevel++;
                    break;
                case ')':
                    if (parenLevel <= 0) {
                        return false;
                    }
                    parenLevel--;
                    break;
            }
        }
        return parenLevel == 0;
    }

    private bool EvaluateMethod(string method, List<string> parameters) {
        //Debug.Log("evaluateMethod called: " + method + " " + parameters.Join("|"));
        switch (method) {
            case "takeItem":
                if (parameters.Count != 1) {
                    Debug.LogWarning("takeItem requires a itemName parameter");
                    return false;
                }
                return TakeItem(parameters[0]);
            case "hasItem":
                if (parameters.Count != 1) {
                    Debug.LogWarning("hasItem requires a itemName parameter");
                    return false;
                }
                return HasItem(parameters[0]);
            case "hasQuest":
                if (parameters.Count != 1) {
                    Debug.LogWarning("hasQuest requires a questId parameter");
                    return false;
                }
                return QuestManager.instance.HasQuest(parameters[0]);
            case "completedQuest":
                if (parameters.Count != 1) {
                    Debug.LogWarning("completedQuest requires a questId parameter");
                    return false;
                }
                return QuestManager.instance.CompletedQuest(parameters[0]);
            case "onQuest":
                if (parameters.Count != 1) {
                    Debug.LogWarning("onQuest requires a questId parameter");
                    return false;
                }
                return QuestManager.instance.OnQuest(parameters[0]);
            case "met":
                var speakerName = DialogRunner.instance.speaker.name;
                return DialogVariables.instance.Get("met" + speakerName);
            case "onObjective":
                if (parameters.Count != 2) {
                    Debug.LogWarning("onObjective requires a questId and order parameter");
                    return false;
                }
                var order = int.Parse(parameters[1]);
                return QuestManager.instance.OnObjective(parameters[0], order);
            case "completedObjective":
                if (parameters.Count != 1) {
                    Debug.LogWarning("completedObjective requires a objectiveName parameter");
                    return false;
                }
                return QuestManager.instance.ObjectiveCompleted(parameters[0]);
        }
        throw new Exception("command doesn't exist: " + method);
    }

    private bool TakeItem(string itemName) {
        var speaker = DialogRunner.instance.speaker;
        if (!(speaker is GameEntity)) {
            Debug.LogWarning(speaker.name + " is not a gameEntity, and cannot contain things");
            return false;
        }

        var gameEntity = (GameEntity)speaker;
        if (!gameEntity.isContainer) {
            Debug.LogWarning(speaker.name + " is not a container");
            return false;
        }

        foreach (var player in TurnManager.instance.Players()) {
            var item = player.GetItemByName(itemName);
            if (item != null) {
                player.Remove(item);
                gameEntity.Add(item);
                return true;
            }
        }
        return false;
    }


    private bool HasItem(string itemName) {
        foreach (var player in TurnManager.instance.Players()) {
            var item = player.GetItemByName(itemName);
            if (item != null) {
                return true;
            }
        }
        return false;
    }
}
