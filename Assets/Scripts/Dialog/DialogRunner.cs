﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class DialogRunner : MonoBehaviour {
    public static DialogRunner instance;

    public DialogData dialogData;
    public DialogPanel view;
    public ManagedPrefab speaker;
    public DialogCommands commands;
    public DialogConditionals dialogConditionals;
    private List<DialogNode> dialog;
    private int currentLineNumber;
    private readonly Stack<int> destinationAfterBlock = new Stack<int>();
    private string[] currentNodeLines;
    public const string startNodeName = "start";

    public delegate void MakeChoice(int choiceId);

    private void Awake() {
        instance = this;
    }

    public void StartDialog(ManagedPrefab _speaker) {
        speaker = _speaker;
        var map = PlayerMapManager.instance.currentMap;
        dialog = dialogData.GetDialog(map, speaker.name);
        if (dialog == null) {
            return;
        }
        if (!GetNode(startNodeName)) {
            Debug.LogWarning("no start node (named: '" + startNodeName +
                             "') was found for " + speaker.name);
            return;
        }
        StartCoroutine(StartDialogCoroutine());
    }

    public void StartDialog(string dialogName, bool locationSpecific) {
        speaker = null;
        var map = locationSpecific ? PlayerMapManager.instance.currentMap : TriggerData.anywhere;
        dialog = dialogData.GetDialog(map, dialogName);
        if (dialog == null) {
            return;
        }
        if (!GetNode(startNodeName)) {
            Debug.LogWarning("no start node (named: '" + startNodeName +
                             "') was found for " + dialogName);
            return;
        }
        StartCoroutine(StartDialogCoroutine());
    }

    private IEnumerator StartDialogCoroutine() {
        view.StartDialog();

        while (currentLineNumber < currentNodeLines.Length) {
            var line = currentNodeLines[currentLineNumber];
            //Debug.Log("running dialog line: " + line);

            int spacesInCurrentBlock = destinationAfterBlock.Count;
            //Debug.Log("spaces expected: " + spacesInCurrentBlock);
            if (!StartsWithSpaces(currentLineNumber, spacesInCurrentBlock)) {
                currentLineNumber = destinationAfterBlock.Pop();
                //Debug.Log("ending block, jumping to line: " + currentLineNumber);
                continue;
            }

            line = line.Substring(spacesInCurrentBlock);
            if (line.Length == 0) {
                //skip blank lines
                currentLineNumber++;
                continue;
            }
            switch (line[0]) {
                case '-':
                    yield return StartCoroutine(DashLine(line.Substring(1)));
                    break;
                case '@':
                    var nodeName = line.Substring(1).Trim();
                    if (nodeName == "end") {
                        //Debug.Log("encountered an end tag -> ending dialog");
                        currentLineNumber = int.MaxValue;
                        break;
                    }
                    if (!GetNode(nodeName)) {
                        Debug.LogWarning("node not found: " + nodeName);
                        currentLineNumber = int.MaxValue;
                    }
                    break;
                case '?':
                    yield return StartCoroutine(ChoiceLine());
                    break;
                default:
                    yield return StartCoroutine(view.ShowDialog(line));
                    currentLineNumber++;
                    break;
            }
        }

        view.EndDialog();
    }

    private IEnumerator ChoiceLine() {
        var choices = new List<string>();
        var choiceStartLineNums = new List<int>();
        var endOfChoices = currentLineNumber;
        var spacesInBlock = destinationAfterBlock.Count + 1;
        while (HasChoice(endOfChoices)) {
            var choice =
                currentNodeLines[endOfChoices].Substring(spacesInBlock).Trim();
            var skip = false;
            var conditional = ChoiceConditional(choice);
            if (conditional != null) {
                var isTrue = dialogConditionals.Evaluate(conditional);
                //Debug.Log("choice conditional: " + conditional + " => " + isTrue);
                skip = !isTrue;
                choice = choice.Substring(conditional.Length).TrimStart();
            }

            endOfChoices++;
            var choiceStart = -1;
            if (StartsWithSpaces(endOfChoices, spacesInBlock)) {
                choiceStart = endOfChoices;
                endOfChoices = EndOfBlock(endOfChoices, spacesInBlock);
            }
            if (skip) {
                //Debug.Log("skipping choice: " + choice + "(" + choiceStart + ")");
            }
            else {
                choices.Add(choice);
                choiceStartLineNums.Add(choiceStart);
                //Debug.Log("choice found: " + choice + "(" + choiceStart + ")");
            } 
        }

        var selected = -1;
        yield return StartCoroutine(
            view.GetChoice(choices, (choiceId) => selected = choiceId));

        while (selected == -1) {
            yield return null;
        }

        var destLine = choiceStartLineNums[selected];
        if (destLine == -1) {
            currentLineNumber = endOfChoices;
            //Debug.Log("choice selected, empty - jumping to end: " + endOfChoices);
        }
        else {
            destinationAfterBlock.Push(endOfChoices);
            currentLineNumber = destLine;
            //Debug.Log("choice selected, jumping to: " + destLine);
        }
    }

    private string ChoiceConditional(string choice) {
        if (!choice.StartsWith("(")) {
            return null;
        }
        var parens = 1;
        for (var i = 1; i < choice.Length; i++) {
            switch (choice[i]) {
                case '(':
                    parens++;
                    break;
                case ')':
                    parens--;
                    if (parens == 0) {
                        return choice.Substring(0, i + 1);
                    }
                    break;
            }
        }
        return null;
    }

    public bool StartsWithSpaces(int lineNum, int spaces) {
        if (lineNum >= currentNodeLines.Length) {
            return false;
        }
        var line = currentNodeLines[lineNum];
        if (line.Length < spaces) {
            return false;
        }
        for (int i = 0; i < spaces; i++) {
            if (line[i] != ' ') {
                return false;
            }
        }
        return true;
    }

    private IEnumerator DashLine(string line) {
        line = line.Trim();
        if (line.StartsWith("if")) {
            var conditional = line.Substring(2);
            var isTrue = dialogConditionals.Evaluate(conditional);
            //Debug.Log("evaluated conditional: " + conditional + " => " + isTrue);
            var spacesInBlock = destinationAfterBlock.Count + 1;
            currentLineNumber++;
            if (isTrue) {
                var endOfIf = EndOfBlock(currentLineNumber, spacesInBlock);
                if (HasElse(endOfIf, destinationAfterBlock.Count)) {
                    endOfIf++;
                    endOfIf = EndOfBlock(endOfIf, spacesInBlock);
                }
                destinationAfterBlock.Push(endOfIf);
            }
            else {
                currentLineNumber = EndOfBlock(currentLineNumber, spacesInBlock);

                if (HasElse(currentLineNumber, destinationAfterBlock.Count)) {
                    currentLineNumber++;
                    var endOfElse = EndOfBlock(currentLineNumber, spacesInBlock);
                    destinationAfterBlock.Push(endOfElse);
                }
            }
        }
        else {
            yield return StartCoroutine(commands.RunCommand(line));
            currentLineNumber++;
        }
    }

    private bool HasElse(int lineNum, int spaces) {
        if (!StartsWithSpaces(lineNum, spaces)) {
            return false;
        }
        var line = currentNodeLines[lineNum];
        if (line.Length < spaces + 5) {
            return false;
        }
        if(line[spaces] != '-') {
            return false;
        }
        return line.Substring(spaces + 1).Trim() == "else";
    }

    private bool HasChoice(int lineNum) {
        var spacesExpected = destinationAfterBlock.Count;
        if (!StartsWithSpaces(lineNum, spacesExpected)) {
            return false;
        }
        var line = currentNodeLines[lineNum];
        return line.Length > spacesExpected && line[spacesExpected] == '?';
    }

    private int EndOfBlock(int startLine, int spacesInBlock) {
        var endOfBlock = startLine;

        while (StartsWithSpaces(endOfBlock, spacesInBlock)) {
            endOfBlock++;
        }
        if (endOfBlock == startLine) {
            throw new Exception("if has nothing in it");
        }
        return endOfBlock;
    }

    private bool GetNode(string nodeName) {
        var node = dialog.First((n) => { return n.name == nodeName; });
        if (node == null) {
            return false;
        }
        currentLineNumber = 0;
        destinationAfterBlock.Clear();
        currentNodeLines = node.dialog.Split('\n');

        return true;
    }
}
