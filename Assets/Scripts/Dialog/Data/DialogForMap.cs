﻿using System;
using System.Collections.Generic;

[Serializable]
public class DialogForMapSpeaker {
    public string mapName;
    public string speakerName;
    public List<DialogNode> dialogNodes;
}
