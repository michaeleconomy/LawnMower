﻿using UnityEngine;

public static class Vector2In3DExtension {
    public static Vector3 In3D(this Vector2 v, float z = 0) {
        return new Vector3(v.x, v.y, z);
    }

    public static Vector3Int In3D(this Vector2Int v, int z = 0) {
        return new Vector3Int(v.x, v.y, z);
    }
}
