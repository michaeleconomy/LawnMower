﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Linq;

public class GameToolsWindow : EditorWindow {
    private bool checkedSave = false;
    private bool saveIsNewer = false;
    private bool wasPlaying = false;

    private AssetCreator.PrefabType prefabType;

    [MenuItem("Window/GameTools")]
    static void Init() {
        var window =
            (GameToolsWindow)EditorWindow.GetWindow(typeof(GameToolsWindow));
        window.Show();
    }

    void OnGUI() {
        var mapSaveManager = FindObjectOfType<MapSaveManager>();

        if (wasPlaying && !Application.isPlaying) {
            checkedSave = false;
        }
        wasPlaying = Application.isPlaying;
        if (!checkedSave) {
            checkedSave = true;
            saveIsNewer = mapSaveManager.LocalSaveNewer();
        }

        if (saveIsNewer) {
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("MapSave Availible")) {
                mapSaveManager.mapManager.tileManager.Reload();
                mapSaveManager.mapManager.prefabManager.Reindex();
                mapSaveManager.Load();
                saveIsNewer = false;
            }
            GUI.backgroundColor = oldColor;
        }

        if (GUILayout.Button("Reindex Assets")) {
            mapSaveManager.mapManager.tileManager.Reload();
            mapSaveManager.mapManager.prefabManager.Reindex();
        }
        
        GUILayout.BeginHorizontal();
        SceneButton("Main", "Assets/Scenes/MainScene.unity");
        SceneButton("Edit", "Assets/Scenes/EditScene.unity");
        GUILayout.EndHorizontal();

        prefabType =
            (AssetCreator.PrefabType)EditorGUILayout.EnumPopup(prefabType);

        var dropped = GUIExtension.DropZone("drop prefab images here");
        if (dropped != null) {
            AssetCreator.CreateAssets(dropped, prefabType);
            mapSaveManager.mapManager.prefabManager.Reindex();
        }

        var mapManager = FindObjectOfType<EditMapManager>();
        if (mapManager != null) {
            var areas = mapManager.MapList().ToList();
            var index = areas.IndexOf(mapManager.currentMap);
            var newIndex = EditorGUILayout.Popup(index, areas.ToArray());
            if (newIndex != index) {
                var newArea = areas[newIndex];
                mapManager.SwitchToEdit(newArea);
            }
        }

    }

    private void SceneButton(string buttonLabel, string scene) {
        GUI.enabled = !scene.Contains(SceneManager.GetActiveScene().name);
        if (GUILayout.Button(buttonLabel)) {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
                EditorSceneManager.OpenScene(scene);
            }
        }
        GUI.enabled = true;
    }
}
