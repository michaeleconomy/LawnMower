﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class TileCreatorWindow : EditorWindow {
    public AssetCreator.TileType mode = AssetCreator.TileType.rule;

    [MenuItem("Window/TileCreator")]
    static void Init() {
        var window =
            (TileCreatorWindow)EditorWindow.GetWindow(typeof(TileCreatorWindow));
        window.Show();
    }

    void OnGUI() {
        mode = (AssetCreator.TileType)EditorGUILayout.EnumPopup(mode);
        var droppedTileImages = GUIExtension.DropZone("drop tile images here");

        if (droppedTileImages != null) {
            AssetCreator.CreateTile(droppedTileImages, mode);

            var mapSaveManager = FindObjectOfType<MapSaveManager>();
            mapSaveManager.mapManager.tileManager.Reload();
        }
    }
}
