﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Item))]
[CanEditMultipleObjects]
public class ItemEditor : GameEntityEditor {

    public override void OnInspectorGUI() {
        var item = (Item)target;

        item.isContainer = EditorGUILayout.BeginToggleGroup("Container?", item.isContainer);
        EditorGUILayout.PropertyField(startingContentsProperty, true);

        if (Application.isPlaying) {
            EditorGUILayout.PropertyField(contentsProperty, true);
        }
        serializedObject.ApplyModifiedProperties();
        EditorGUILayout.EndToggleGroup();

        var weightString = EditorGUILayout.TextField("Weight (ounces)", item.individualWeight.ToString());
        try {
            if (weightString.EndsWith("lbs")) {
                var weightLbs = weightString.Substring(0, weightString.Length - 3);
                item.individualWeight = float.Parse(weightLbs) * 16;
            }
            else {
                item.individualWeight = float.Parse(weightString);
            }
        }
        catch (FormatException) {
            item.individualWeight = 0f;
        }

        item.value = EditorGUILayout.FloatField("Value (gp)", item.value);
        item.questItem = EditorGUILayout.Toggle("Quest Item", item.questItem);

        if (GUI.changed) {
            EditorUtility.SetDirty(item);
        }
    }
}
