﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pile))]
public class PileEditor : GameEntityEditor {

    SerializedProperty spritesProperty;

    protected override void OnEnable() {
        base.OnEnable();
        spritesProperty = serializedObject.FindProperty("sprites");
    }

    public override void OnInspectorGUI() {
        var pile = (Pile)target;
        pile.Quantity = EditorGUILayout.IntField("Quantity", pile.Quantity);
        EditorGUILayout.PropertyField(spritesProperty, true);

        var weightString = EditorGUILayout.TextField("Weight Per (ounces)", pile.individualWeight.ToString());
        try {
            if (weightString.EndsWith("lbs")) {
                var weightLbs = weightString.Substring(0, weightString.Length - 3);
                pile.individualWeight = float.Parse(weightLbs) * 16;
            }
            else {
                pile.individualWeight = float.Parse(weightString);
            }
        }
        catch (FormatException) {
            pile.individualWeight = 0f;
        }

        serializedObject.ApplyModifiedProperties();
        pile.value = EditorGUILayout.FloatField("Value Per (gp)", pile.value);

        if (GUI.changed) {
            EditorUtility.SetDirty(pile);
        }
    }
}
