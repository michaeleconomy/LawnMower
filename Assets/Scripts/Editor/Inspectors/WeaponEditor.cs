﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(Weapon))]
[CanEditMultipleObjects]
public class WeaponEditor : ItemEditor {

    readonly string[] diceNames = { "d4", "d6", "d8", "d10", "d12", "d20" };
    readonly int[] diceSides = { 4, 6, 8, 10, 12, 20 };
    SerializedProperty attachLocationProperty;

    protected override void OnEnable() {
        base.OnEnable();
        attachLocationProperty = serializedObject.FindProperty("attachLocation");
    }

    public override void OnInspectorGUI() {
        var weapon = (Weapon)target;

        //Should be defaulted by the reset method i added
        //myTarget.slot = (Equipment.Slot)EditorGUILayout.EnumPopup("Slot", myTarget.slot);

        weapon.numDamageDice = EditorGUILayout.IntField("Num Damage Dice", weapon.numDamageDice);
        weapon.damageDiceSize = EditorGUILayout.IntPopup("Damage Dice Size", weapon.damageDiceSize, diceNames, diceSides);
        weapon.damageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", weapon.damageType);
        EditorGUILayout.Space();
        weapon.type = (Weapon.Type)EditorGUILayout.EnumPopup("Type", weapon.type);

        var ranged = EditorGUILayout.BeginToggleGroup("Ranged", weapon.range > 0);
        if (ranged) {
            if (weapon.range <= 0) {
                weapon.range = 1;
            }
            weapon.range = EditorGUILayout.IntField("Range", weapon.range);
            weapon.longRange = EditorGUILayout.IntField("Long Range", weapon.longRange);
            weapon.thrown = EditorGUILayout.Toggle("Thrown", weapon.thrown);
            weapon.loading = EditorGUILayout.Toggle("Loading", weapon.loading);
            weapon.reach = true;
        }
        else if(!ranged) {
            weapon.thrown = false;
            weapon.range = 0;
            weapon.longRange = 0;
            weapon.loading = false;
        }

        EditorGUILayout.EndToggleGroup();
        if (!ranged) {
            weapon.reach = EditorGUILayout.Toggle("Reach", weapon.reach);
        }
        weapon.versatile = EditorGUILayout.BeginToggleGroup("Versatile", weapon.versatile);
        weapon.versatileDiceSize = EditorGUILayout.IntPopup("Versatile Damage Dice Size", weapon.versatileDiceSize, diceNames, diceSides);
        EditorGUILayout.EndToggleGroup();

        weapon.light = EditorGUILayout.Toggle("Light", weapon.light);
        weapon.heavy = EditorGUILayout.Toggle("Heavy", weapon.heavy);
        weapon.finesse = EditorGUILayout.Toggle("Finesse", weapon.finesse);
        weapon.twoHanded = EditorGUILayout.Toggle("Two-handed", weapon.twoHanded);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(attachLocationProperty, true);

        serializedObject.ApplyModifiedProperties();
        base.OnInspectorGUI();
    }
}
