﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

[CustomEditor(typeof(ManagedPrefab))]
[CanEditMultipleObjects]
public class ManagedPrefabEditor : Editor {
    SerializedProperty randomSpritesProperty;
    SerializedProperty prefabIdProperty;
    SerializedProperty genericNameProperty;
    SerializedProperty hasDialogProperty;
    SerializedProperty activeProperty;
    SerializedProperty scalableProperty;
    SerializedProperty freePlaceProperty;
    SerializedProperty disableColliderProperty;
    SerializedProperty varyColorProperty;
    SerializedProperty colorProperty;
    SerializedProperty minColorProperty;
    SerializedProperty maxColorProperty;
    SerializedProperty randomSpriteProperty;
    SerializedProperty randomSpriteIndexProperty;

    protected virtual void OnEnable() {
        prefabIdProperty = serializedObject.FindProperty("prefabId");
        genericNameProperty = serializedObject.FindProperty("genericName");
        hasDialogProperty = serializedObject.FindProperty("hasDialog");
        activeProperty = serializedObject.FindProperty("active");
        scalableProperty = serializedObject.FindProperty("scalable");
        freePlaceProperty = serializedObject.FindProperty("freePlace");
        disableColliderProperty = serializedObject.FindProperty("disableCollider");
        varyColorProperty = serializedObject.FindProperty("varyColor");
        colorProperty = serializedObject.FindProperty("Color");
        minColorProperty = serializedObject.FindProperty("minColor");
        maxColorProperty = serializedObject.FindProperty("maxColor");
        randomSpriteProperty = serializedObject.FindProperty("randomSprite");
        randomSpriteIndexProperty = serializedObject.FindProperty("randomSpriteIndex");
        randomSpritesProperty = serializedObject.FindProperty("randomSprites");
    }

    public override void OnInspectorGUI() {
        var managedPrefab = (ManagedPrefab)target;
        EditorGUILayout.PropertyField(prefabIdProperty);
        EditorGUILayout.PropertyField(genericNameProperty);
        EditorGUILayout.PropertyField(hasDialogProperty, true);
        EditorGUILayout.PropertyField(activeProperty, true);
        EditorGUILayout.PropertyField(scalableProperty, true);
        EditorGUILayout.PropertyField(freePlaceProperty, true);
        EditorGUILayout.PropertyField(disableColliderProperty, true);
        EditorGUILayout.PropertyField(varyColorProperty, true);
        if (managedPrefab.varyColor) {
            EditorGUILayout.PropertyField(colorProperty, true);
            EditorGUILayout.PropertyField(minColorProperty, true);
            EditorGUILayout.PropertyField(maxColorProperty, true);
            if (GUILayout.Button("SetRandomColor")) {
                managedPrefab.SetRandomColor();
                colorProperty.colorValue = managedPrefab.Color;
            }
        }

        EditorGUILayout.PropertyField(randomSpriteProperty, true);
        if (managedPrefab.randomSprite) {
            EditorGUILayout.PropertyField(randomSpriteIndexProperty, true);
            EditorGUILayout.PropertyField(randomSpritesProperty, true);
        }
        serializedObject.ApplyModifiedProperties();
    }
}