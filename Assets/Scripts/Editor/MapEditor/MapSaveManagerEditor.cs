﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapSaveManager))]
public class MapSaveManagerEditor : Editor {

    public override void OnInspectorGUI() {
        var mapSaveManager = (MapSaveManager)target;

        base.OnInspectorGUI();
        if (GUILayout.Button("Load")) {
            mapSaveManager.Load();
        }
    }
}