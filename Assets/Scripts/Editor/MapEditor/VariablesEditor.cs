﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

[CustomEditor(typeof(DialogVariables))]
public class VariablesEditor : Editor {
    public string newField;
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var variables = (DialogVariables)target;
        string variableToRemove = null;
        foreach (var variable in variables.variables) {
            GUILayout.BeginHorizontal();
            GUILayout.Label(variable);
            if(GUILayout.Button("remove")) {
                variableToRemove = variable;
            }
            GUILayout.EndHorizontal();
        }
        if (variableToRemove != null) {
            variables.Remove(variableToRemove);

        }
        GUILayout.BeginHorizontal();
        newField = EditorGUILayout.TextField("new", newField);
        if(GUILayout.Button("add")) {
            variables.Set(newField);
        }
        GUILayout.EndHorizontal();

        if (GUI.changed) {
            EditorUtility.SetDirty(variables);
        }
    }
}