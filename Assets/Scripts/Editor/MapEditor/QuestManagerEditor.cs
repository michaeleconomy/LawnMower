﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(QuestManager))]
public class QuestManagerEditor : Editor {
    bool showObjectives;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var questManager = (QuestManager)target;
        showObjectives = EditorGUILayout.Foldout(showObjectives, "ObjectivesCompleted");
        if (showObjectives){
            foreach (var objective in questManager.objectivesCompleted) {
                EditorGUILayout.LabelField(objective, "true");
            }
        }
    }
}