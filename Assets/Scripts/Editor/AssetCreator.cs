﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public static class AssetCreator {
    public enum TileType {
        plain,
        random,
        rule
    }

    public enum PrefabType {
        generic,
        weapon,
        armor
    }

    public static void CreateAssets(UnityEngine.Object[] objects, PrefabType type) {
        //TODO detect if they are a set (tiles)
        foreach (var obj in objects) {
            CreateAsset(obj, type);
        }
    }


    public static void CreateAsset(UnityEngine.Object obj, PrefabType type) {
        if (obj is Texture2D) {
            switch(type) {
                case PrefabType.generic:
                    CreateManagedPrefab((Texture2D)obj);
                    return;
                case PrefabType.weapon:
                    CreateWeapon((Texture2D)obj);
                    return;
                default:
                    Debug.LogWarning("don't know how to create type: " + type);
                    return;
            }
        }
        else {
            var path = AssetDatabase.GetAssetPath(obj);
            Debug.LogWarning("don't know how to create asset from: " + path + " - " + obj);
        }
    }

    private static void CreateWeapon(Texture2D texture) {
        CreateBase(texture, (go) => {
            go.AddComponent<Weapon>();
        });
    }

    private delegate void CreateAdditional(GameObject go);
    private static void CreateBase(Texture2D texture, CreateAdditional additional) {
        var path = AssetDatabase.GetAssetPath(texture);

        var prefab = new GameObject(GetName(path));

        var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
        var spriteRenderer = prefab.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
        spriteRenderer.sortingLayerName = "items";

        prefab.AddComponent<BoxCollider2D>();
        additional(prefab);
        prefab.AddComponent<ManagedPrefabTouchHandler>();
        var outline = prefab.AddComponent<OutlineObject>();
        outline.enabled = false;
        outline.outlineMaterial =
            AssetDatabase.LoadAssetAtPath<Material>(
              "Assets/Libraries/OutlineObject/Materials/Sprites-Outline.mat");
        outline.outlineSize = 6;
        outline.outlineBlur = 3;

        var outputPath = DestinationAssetPath(path);
        DirectoryHelper.CreateForPath(outputPath);
        PrefabUtility.CreatePrefab(outputPath, prefab);
        UnityEngine.Object.DestroyImmediate(prefab);
        Debug.Log("created asset: " + outputPath);
    }

    private static void CreateManagedPrefab(Texture2D texture) {
        CreateBase(texture, (go) => {
            go.AddComponent<ManagedPrefab>();
        });
    }


    private static Regex nameRegex = new Regex(@".*/(?<name>[\w\s-]+)\..*");
    private static Regex nonWordCharacters = new Regex(@"[^\w]+");
    private static string GetName(string path) {
        var match = nameRegex.Match(path);
        if (!match.Success) {
            throw new Exception("Coudlnt' determine name from path: " + path);
        }

        var rawName = match.Groups["name"].Value;
        return nonWordCharacters.Replace(rawName, "_");
    }

    private static string DestinationAssetPath(string inputPath) {
        //Assets/Sprites/Areas/Town/misc/sign post back.png
        //Assets/GameObjects/Areas/Town/misc/sign_post_back
        var updatedPath = inputPath.Replace("Sprites", "GameObjects").Replace(".png", ".prefab");
        var match = nameRegex.Match(updatedPath);
        if (!match.Success) {
            throw new Exception("Coudlnt' determine name from path: " + inputPath);
        }

        var rawName = match.Groups["name"].Value;
        var updatedName = nonWordCharacters.Replace(rawName, "_");
        return updatedPath.Replace(rawName, updatedName);
    }

    public static void CreateTile(UnityEngine.Object[] objects, TileType mode) {
        var sprites = new List<Sprite>();
        foreach (var o in objects) {
            if (o is Texture2D) {
                var path = AssetDatabase.GetAssetPath(o);

                var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
                sprites.Add(sprite);
            }
            else {
                Debug.LogWarning("unknown type: " + o.GetType() + " - " + o);
            }
        }
        if (sprites.Count > 0) {
            switch (mode) {
                case TileType.plain:
                    MakePlainTile(sprites);
                    return;
                case TileType.random:
                    MakeRandomTile(sprites);
                    return;
                case TileType.rule:
                    MakeRuleTile(sprites);
                    return;
            }
        }
    }

    private static void MakeRuleTile(List<Sprite> sprites) {
        var ruleTile = ScriptableObject.CreateInstance<RuleTile>();
        var first = sprites.First();

        ruleTile.m_DefaultSprite = first;
        ruleTile.m_TilingRules = new List<RuleTile.TilingRule>();
        foreach (var sprite in sprites) {
            var tilingRule = new RuleTile.TilingRule();
            if (ruleTile.m_DefaultSprite == null) {
            }
            tilingRule.m_Sprites = new[] { sprite };

            ruleTile.m_TilingRules.Add(tilingRule);
        }

        var path = AssetDatabase.GetAssetPath(first);
        var outputPath = TileOutputPath(path);
        DirectoryHelper.CreateForPath(outputPath);
        AssetDatabase.CreateAsset(ruleTile, outputPath);
    }

    private static Regex pathRegex = new Regex(@"Assets/Sprites/(?<main>.*)/.*");
    private static string TileOutputPath(string path) {
        var match = pathRegex.Match(path);
        if (!match.Success) {
            throw new Exception("regex didn't match path: " + path);
        }
        return "Assets/GameObjects/" + match.Groups["main"].Value + ".asset";
    }

    private static void MakeRandomTile(List<Sprite> sprites) {
        throw new NotImplementedException();
    }

    private static void MakePlainTile(List<Sprite> sprites) {
        throw new NotImplementedException();
    }

}
