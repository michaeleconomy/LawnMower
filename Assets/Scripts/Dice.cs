﻿using UnityEngine;
public static class Dice {
    public static int Roll(int numDice, int numSides) {
        if (numDice < 0) {
            throw new System.Exception("numDice cannot be less than zero");
        }

        if (numDice < 1)
        {
            throw new System.Exception("numSides cannot be less than one");
        }
        var roll = 0;
        for (var i = 0; i < numDice; i++) {
            roll += Random.Range(1, numSides);
        }
        return roll;
    }

    public static float AverageRoll(int numDice, int numSides) {
        if (numDice < 0) {
            throw new System.Exception("numDice cannot be less than zero");
        }

        if (numDice < 1) {
            throw new System.Exception("numSides cannot be less than one");
        }

        return numDice * (numSides + 1.0f) / 2;
    }
}
