﻿using System;
using UnityEngine;

public class FillTool : MapTool {
    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        var selectedObject = EditObjectPalette.SelectedObject();
        if (!(selectedObject is TileInfo)) {
            return;
        }
        var positionInt = Vector2Int.FloorToInt(position);
        MapEditor.instance.FillTiles(positionInt, (TileInfo)selectedObject);
    }
}
