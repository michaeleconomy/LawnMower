﻿using System;
using UnityEngine;

public class SprayPaintTool : MapTool {
    public float radius = 2;

    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        var selectedObject = EditObjectPalette.SelectedObject();
        if (!(selectedObject is PrefabInfo)) {
            return;
        }
        if (managedPrefab != null) {
            if (managedPrefab.prefabId == selectedObject.id) {
                return;
            }
        }
        var prefabInfo = (PrefabInfo)selectedObject;
        var prefab = prefabInfo.managedPrefab;
        if (!prefab.freePlace) {
            return;
        }
        var name = GetNextPrefabName(prefab);
        var spawnedGameEntity =
            UnityEngine.Object.Instantiate(prefab, World.instance.transform);
        spawnedGameEntity.name = name;
        if (spawnedGameEntity.varyColor) {
            spawnedGameEntity.SetRandomColor();
        }
        if (spawnedGameEntity.randomSprite) {
            spawnedGameEntity.SetRandomSprite();
        }

        var xOffset = UnityEngine.Random.Range(-radius, radius);
        var yOffset = UnityEngine.Random.Range(-radius, radius);

        spawnedGameEntity.transform.position =
                             new Vector3(position.x + xOffset,
                                         position.y + yOffset, 0);
        return;
    }


    private string GetNextPrefabName(ManagedPrefab prefab) {
        var allPrefabs = World.instance.GetComponentsInChildren<ManagedPrefab>();
        int existingCount = 0;
        foreach (var otherPrefab in allPrefabs) {
            if (otherPrefab.prefabId == prefab.prefabId) {
                existingCount++;
            }
        }
        if (existingCount > 0) {
            return prefab.name + existingCount;
        }
        return prefab.name;
    }

    public override void ToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        UseTool(position, managedPrefab);
    }
}
