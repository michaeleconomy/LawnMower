﻿using System;
using UnityEngine;

public class DropperTool : MapTool {
    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        EditObjectInfo info;
        if (managedPrefab != null) {
            info = EditMapManager.instance.prefabManager.GetInfoById(
                managedPrefab.prefabId);
        }
        else {
            var positionInt = Vector2Int.FloorToInt(position);
            var tile = MapEditor.instance.GetTopTile(positionInt);
            info = EditMapManager.instance.tileManager.GetInfoForTile(tile);
        }
        if (info != null) {
            EditObjectPalette.instance.SelectObject(info);
            MapEditor.instance.SelectBrush();
        }
    }
}
