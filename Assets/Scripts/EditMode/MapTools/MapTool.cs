﻿using System;
using UnityEngine;

public abstract class MapTool {
    public abstract void UseTool(Vector2 position, ManagedPrefab managedPrefab);
    public virtual void StartToolDrag(Vector2 position, ManagedPrefab managedPrefab) { }
    public virtual void EndToolDrag(Vector2 position, ManagedPrefab managedPrefab) { }
    public virtual void ToolDrag(Vector2 position, ManagedPrefab managedPrefab) { }
}
