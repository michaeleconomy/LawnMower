﻿using System;
using UnityEngine;

public class CursorTool : MapTool {
    public ManagedPrefab selected;
    private Vector2 startDragPosition;

    public override void UseTool(Vector2 position, ManagedPrefab managedPrefab) {
        if (managedPrefab == null) {
            Deselect();
            EditEntityDetails.instance.gameObject.SetActive(false);
            return;
        }
        if (managedPrefab == selected) {
            MapEditor.instance.ShowDetails(managedPrefab);
            return;
        }

        Select(managedPrefab);
    }

    public void Select(ManagedPrefab managedPrefab) {
        Deselect();
        selected = managedPrefab;
        selected.EnableOutline(Color.white);
    }

    public void Deselect() {
        if (selected == null) {
            return;
        }
        selected.HideOutline();
        selected = null;
    }

    public override void StartToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        if (managedPrefab == null) {
            Deselect();
            return;
        }
        Select(managedPrefab);
    }

    public override void ToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        if (selected == null) {
            return;
        }
        selected.transform.position = position;
    }

    public override void EndToolDrag(Vector2 position, ManagedPrefab managedPrefab) {
        if (selected == null) {
            return;
        }
        if (!selected.freePlace) {
            selected.transform.position =
                        selected.transform.position.RoundPosition();
        }
    }
}
