﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public abstract class EditObjectSource : ScriptableObject {

    private Dictionary<int, EditObjectInfo> infosById;
    private Dictionary<string, EditObjectInfo> infosByPath;

    private void Index() {
        infosById = new Dictionary<int, EditObjectInfo>();
        infosByPath = new Dictionary<string, EditObjectInfo>();
        foreach (var info in GetAllObjects()) {
            infosById[info.id] = info;
            infosByPath[info.path] = info;
        }
    }

    public EditObjectInfo GetInfoByPath(string path) {
        if (infosByPath == null) {
            Index();
        }
        EditObjectInfo info;
        infosByPath.TryGetValue(path, out info);
        return info;
    }

    public EditObjectInfo GetInfoById(int id) {
        if (infosById == null) {
            Index();
        }
        EditObjectInfo info;
        infosById.TryGetValue(id, out info);
        return info;
    }

    protected List<EditObjectInfo> GetInfosByPath(IEnumerable<string> infoPaths) {
        var list = new List<EditObjectInfo>();
        foreach (var path in infoPaths) {
            list.Add(GetInfoByPath(path));
        }
        return list;
    }

    protected List<EditObjectInfo> GetInfosById(IEnumerable<int> infoIds) {
        var list = new List<EditObjectInfo>();
        foreach (var id in infoIds) {
            list.Add(GetInfoById(id));
        }
        return list;
    }

    public ChooserPathInfo GetAllForPath(string path) {
        var infos = new List<EditObjectInfo>();
        var subDirectories = new HashSet<string>();

        foreach (var info in GetAllObjects()) {
            if (!info.path.StartsWith(path)) {
                continue;
            }
            infos.Add(info);
            var relativePath = info.path.Substring(path.Length);
            var slashIndex = relativePath.IndexOf("/");
            if (slashIndex >= 0) {
                subDirectories.Add(relativePath.Substring(0, slashIndex));
            }

        }
        return new ChooserPathInfo {
            infos = infos,
            subDirectories = subDirectories
        };
    }

    public abstract IEnumerable<EditObjectInfo> GetAllObjects();
}
