﻿using System;

[Serializable]
public abstract class EditObjectInfo {
    public string path;
    public int id;
}
