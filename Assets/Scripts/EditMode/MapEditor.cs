﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapEditor : MonoBehaviour {
    public static MapEditor instance;

    public Grid grid;

    public readonly List<Tilemap> tilemaps = new List<Tilemap>();

    public MapTool selectedTool;
    public MapTool lastTool;

    public BrushTool brushTool = new BrushTool();
    public EraseTool eraseTool = new EraseTool();
    public CursorTool cursorTool = new CursorTool();
    public DropperTool dropperTool = new DropperTool();
    public FillTool fillTool = new FillTool();
    public SprayPaintTool sprayPaintTool = new SprayPaintTool();

    public IMapEditorToolDelegate toolDelegate;
    public IMapEditorEntityDetailsDelegate detailsDelegate;
    public GameObject inactiveMarkerPrefab;

    private void Awake() {
        if (grid == null) {
            throw new Exception("Grid is required");
        }
        for (var i = 0; i < grid.transform.childCount; i++) {
            var tilemapTransform = grid.transform.GetChild(i);
            tilemaps.Add(tilemapTransform.GetComponent<Tilemap>());
        }

        selectedTool = cursorTool;
        instance = this;
    }

    public static GameObject InactiveMarkerPrefab() {
        if (instance == null) {
            return null;
        }
        return instance.inactiveMarkerPrefab;
    }

    public void SetTile(Vector2Int position, TileInfo tileInfo) {
        var tilemap = tilemaps[tileInfo.layer];

        tilemap.SetTile(position.In3D(), tileInfo.tileBase);
    }

    public void SelectBrush() {
        SelectTool(brushTool);
    }

    public void SelectErasor() {
        SelectTool(eraseTool);
    }

    public void SelectCursor() {
        SelectTool(cursorTool);
    }

    public void SelectDropper() {
        SelectTool(dropperTool);
    }

    public void SelectFill() {
        if (!(EditObjectPalette.SelectedObject() is TileInfo)) {
            return;
        }
        SelectTool(fillTool);
    }

    public void SelectSprayPaint() {
        var selectedObject = EditObjectPalette.SelectedObject();
        if (!(selectedObject is PrefabInfo)) {
            return;
        }
        var prefabInfo = (PrefabInfo)selectedObject;
        if (!prefabInfo.managedPrefab.freePlace) {
            return;
        }
        SelectTool(sprayPaintTool);
    }

    public void ToggleGrid() {
        var actionOverlay = ActionOverlay.instance;
        if (actionOverlay.visible) {
            actionOverlay.Clear();
            return;
        }
        actionOverlay.ShowEditMode();
    }

    public void RemoveTopTile(Vector2Int position) {
        var p3d = position.In3D();
        for (var i = tilemaps.Count - 1; i >= 0; i--) {
            var tilemap = tilemaps[i];
            if (tilemap.HasTile(p3d)) {
                tilemap.SetTile(p3d, null);
                return;
            }
        }
    }

    public void RemoveTileOnLayer(Vector2Int position, int layer) {
        var p3d = position.In3D();
        var tilemap = tilemaps[layer];
        tilemap.SetTile(p3d, null);
    }


    public int GetTopTileLayer(Vector2Int position) {
        var p3d = position.In3D();
        for (var i = tilemaps.Count - 1; i >= 0; i--) {
            if (tilemaps[i].HasTile(p3d)) {
                return i;
            }
        }
        return -1;
    }

    public void FillTiles(Vector2Int position, TileInfo tileInfo) {
        var tilemap = tilemaps[tileInfo.layer];
        tilemap.FloodFill(position.In3D(), tileInfo.tileBase);
    }


    public TileBase GetTopTile(Vector2Int position) {
        var p3d = position.In3D();
        for (var i = tilemaps.Count - 1; i >= 0; i--) {
            var tilemap = tilemaps[i];
            var tile = tilemap.GetTile(p3d);
            if (tile != null) {
                return tile;
            }
        }
        return null;
    }


    public GameEntity GameEntityAt(Vector2 position) {
        int mask = LayerMask.GetMask("players", "enemies", "items");
        var boxSize = Vector2.one / 2;
        var boxRotation = 0f;
        var colliderHit = Physics2D.OverlapBox(position.RoundPosition(), boxSize, boxRotation, mask);
        if (colliderHit == null) {
            return null;
        }
        var gameEntity = colliderHit.GetComponent<GameEntity>();
        if (gameEntity == null) {
            Debug.LogWarning("encountered non GameEntity: " + colliderHit.name);
        }
        return gameEntity;
    }

    public void SelectTool(MapTool tool) {
        if (tool == selectedTool) {
            return; //already selected
        }
        lastTool = selectedTool;
        selectedTool = tool;
        if (toolDelegate != null) {
            toolDelegate.ToolSelected(selectedTool);
        }
    }

    public void SwitchToLastTool() {
        SelectTool(lastTool);
    }

    public void ShowDetails(ManagedPrefab managedPrefab) {
        detailsDelegate.ShowDetails(managedPrefab);
    }
}
