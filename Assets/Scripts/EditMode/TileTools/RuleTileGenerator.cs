﻿#if UNITY_EDITOR
using System;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.IO;
using System.Collections.Generic;

public static class RuleTileGenerator {
    public static string inputDirectoryPath = "Assets/Tiles/Base";
    public static Regex ruleFolderRegex = new Regex(@"_Rule/([\w\s]+)");

    public static void GenerateAll() {
        var directories = Directory.GetDirectories(inputDirectoryPath,
                                                   "*",
                                                   SearchOption.AllDirectories);
        foreach (var directory in directories) {
            if (!ruleFolderRegex.IsMatch(directory)) {
                continue;
            }
            Generate(directory);
        }
    }

    private static void Generate(string inputTilesDirectory) {
        Debug.Log("generating ruleTile for: " + inputTilesDirectory);
        var outputPath = GetOutputPath(inputTilesDirectory);

        var existing =
            AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(outputPath);
        if (existing != null) {
            Debug.Log("Asset already exists (" + outputPath + ") not generating");
            return;
        }
        var tilesGuids =
            AssetDatabase.FindAssets("t:Tile", new[] { inputTilesDirectory });
        var ruleTile = ScriptableObject.CreateInstance<RuleTile>();
        ruleTile.m_TilingRules = new List<RuleTile.TilingRule>();
        foreach (var guid in tilesGuids) {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var tile = AssetDatabase.LoadAssetAtPath<Tile>(path);
            var tilingRule = new RuleTile.TilingRule();
            if (ruleTile.m_DefaultSprite == null) {
                ruleTile.m_DefaultSprite = tile.sprite;
            }
            tilingRule.m_Sprites = new[] { tile.sprite };

            ruleTile.m_TilingRules.Add(tilingRule);
        }

        AssetDatabase.CreateAsset(ruleTile, outputPath);
    }

    private static string GetOutputPath(string inputTilesDirectory) {
        var match = ruleFolderRegex.Match(inputTilesDirectory);
        var name = match.Groups[1].Value;
        return ruleFolderRegex.Replace(inputTilesDirectory, name + ".asset");
    }
}
#endif