﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
public class TileInfo : EditObjectInfo {
    public TileBase tileBase;
    public int layer;

    public Sprite PreviewSprite() {
        if (tileBase == null) {
            return null;
        }

        if (tileBase is Tile) {
            if (tileBase is RandomTile) {
                var randomTile = (RandomTile)tileBase;
                return randomTile.m_Sprites[0];
            }
            else {
                var tile = (Tile)tileBase;
                return tile.sprite;
            }
        }
        else if (tileBase is RuleTile) {
            var ruleTile = (RuleTile)tileBase;
            return ruleTile.m_DefaultSprite;
        }
        else {
            Debug.LogError("don't know how to render tile type: " + tileBase.GetType());
        }
        return null;
    }
}
