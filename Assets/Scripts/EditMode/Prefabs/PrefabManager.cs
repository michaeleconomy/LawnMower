﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PrefabManager : EditObjectSource {
    public List<PrefabInfo> prefabs;

    public override IEnumerable<EditObjectInfo> GetAllObjects() {
        return prefabs.Cast<EditObjectInfo>();
    }

    public List<T> GetPrefabsByIds<T>(IEnumerable<int> ids)
            where T : ManagedPrefab {
        var list = new List<T>();
        foreach (var info in GetInfosById(ids)) {
            var prefabInfo = (PrefabInfo)info;
            list.Add((T)prefabInfo.managedPrefab);
        }
        return list;
    }

    public List<T> GetAllPrefabsOfType<T>() where T : ManagedPrefab {
        var list = new List<T>();
        foreach (var info in prefabs) {
            if (info.managedPrefab is T) {
                list.Add((T)info.managedPrefab);
            }
        }
        return list;
    }

#if UNITY_EDITOR

    public static readonly string inputDirectoryPath = "Assets/GameObjects";

    public void Reindex() {
        Debug.Log("reindexing prefabs");
        prefabs = new List<PrefabInfo>();
        var needsId = new List<PrefabInfo>();
        var guids = AssetDatabase.FindAssets("t:Prefab",
                                             new[] { inputDirectoryPath });
        var idsUsed = new HashSet<int>();
        foreach (var guid in guids) {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var prefab = AssetDatabase.LoadAssetAtPath<ManagedPrefab>(path);
            if (prefab == null) {
                continue;
            }
            var info = new PrefabInfo {
                managedPrefab = prefab,
                path = CleanPath(path),
                id = prefab.prefabId
            };
            prefabs.Add(info);
            if (prefab.prefabId == 0 || idsUsed.Contains(prefab.prefabId)) {
                needsId.Add(info);
            }
            else {
                idsUsed.Add(prefab.prefabId);
            }
        }

        var nextId = 1;
        foreach (var info in needsId) {
            while(idsUsed.Contains(nextId)) {
                nextId++;
            }
            info.id = nextId;
            var prefab = info.managedPrefab;
            prefab.prefabId = nextId;
            EditorUtility.SetDirty(prefab);
            nextId++;
        }
        EditorUtility.SetDirty(this);
        Debug.Log("done reindexing prefabs");
    }


    private string CleanPath(string path) {
        return path.Replace(inputDirectoryPath, "").
                   Substring(1).
                   Replace(".prefab", "");
    }

#endif
}
