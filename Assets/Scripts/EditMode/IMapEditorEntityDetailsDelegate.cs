﻿using System;
public interface IMapEditorEntityDetailsDelegate {
    void ShowDetails(ManagedPrefab managedPrefab);
}
