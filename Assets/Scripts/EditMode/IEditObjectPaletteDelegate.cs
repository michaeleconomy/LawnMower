﻿using System;
using System.Collections.Generic;

public interface IEditObjectPaletteDelegate {
    void ObjectsUpdated(EditObjectInfo selectedObject,
                        List<EditObjectInfo> recentObjects);
}
