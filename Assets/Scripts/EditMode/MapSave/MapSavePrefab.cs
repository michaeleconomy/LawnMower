﻿using System;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[Serializable]
public class MapSavePrefab {
    public int id;
    public string name;
    public bool active;
    public bool trackDeath;
    public float x;
    public float y;
    public float scale;
    public List<int> startingContentsPrefabIds;
    public List<int> startingEquipmentPrefabIds;
    public SavedHumanoidBody savedHumanoidBody;
    public SavedColor color;
    public int randomSpriteIndex;

    public static MapSavePrefab Save(ManagedPrefab managedPrefab) {
        var position = managedPrefab.transform.position;
        var save = new MapSavePrefab {
            name = managedPrefab.name,
            active = managedPrefab.active,
            x = position.x,
            y = position.y,
            scale = managedPrefab.transform.localScale.x,
            id = managedPrefab.prefabId
        };

        if (managedPrefab is GameEntity) {
            var gameEntity = (GameEntity)managedPrefab;
            save.startingContentsPrefabIds =
                gameEntity.startingContents.Select(item => item.prefabId).ToList();
            if (gameEntity is Character) {
                var character = (Character)gameEntity;
                save.startingEquipmentPrefabIds =
                    character.startingEquipment.Select(item => item.prefabId).
                             ToList();
                if (character is NonPlayerCharacter) {
                    var npc = (NonPlayerCharacter)character;
                    save.trackDeath = npc.trackDeath;
                }
                var humanoidBody = character.GetComponent<HumanoidBody>();
                if (humanoidBody != null) {
                    save.savedHumanoidBody = new SavedHumanoidBody(humanoidBody);
                }
            }
        }

        if (managedPrefab.varyColor) {
            save.color = new SavedColor(managedPrefab.Color);
        }
        if (managedPrefab.randomSprite) {
            save.randomSpriteIndex = managedPrefab.RandomSpriteIndex;
        }
        return save;
    }

}
