﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.S3;
using Amazon.S3.Model;
using UnityEngine;

public class S3Client : MonoBehaviour {

    public static S3Client instance;

    public delegate void ListFilesResponseDelegate(IEnumerable<FileListItem> files);
    public delegate void ReadFileResponseDelegate(string file);
    public delegate void WriteFileResponseDelegate(bool success);

    //public delegate void WriteFileResponseDelegate();

    public class FileListItem {
        public string fileName;
        public DateTime lastModified;
    }

    private AmazonS3Client client;
    public string bucketName = "michaels-rpg-bucket";

    private void Awake() {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;
        UnityInitializer.AttachToGameObject(gameObject);
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        //var loggingConfig = AWSConfigs.LoggingConfig;
        //loggingConfig.LogTo = LoggingOptions.UnityLogger;
        //loggingConfig.LogMetrics = true;
        //loggingConfig.LogResponses = ResponseLoggingOption.Always;
        //loggingConfig.LogResponsesSizeLimit = 4096;
        //loggingConfig.LogMetricsFormat = LogMetricsFormatOption.JSON;


        var region = RegionEndpoint.USWest2;
        var identityPoolId = "us-west-2:decb40c9-001a-4ecd-93e2-377d7dceaa8b";
        var credentials = new CognitoAWSCredentials(identityPoolId, region);
        client = new AmazonS3Client(credentials, region);
    }

    public void ListFiles(ListFilesResponseDelegate responseDelegate) {
        var request = new ListObjectsRequest() {
            BucketName = bucketName
        };

        client.ListObjectsAsync(request, (responseObj) => {
            if (responseObj.Exception != null) {
                Debug.LogWarning("s3 exception: " + responseObj.Exception.Message);
                return;
            }
            var fileNames = new List<FileListItem>();
            foreach (var fileInfo in responseObj.Response.S3Objects) {
                fileNames.Add(new FileListItem {
                    fileName = fileInfo.Key,
                    lastModified = fileInfo.LastModified
                });
            }

            responseDelegate(fileNames);
        });
    }

    public void ReadFile(string fileName, ReadFileResponseDelegate responseDelegate) {
        client.GetObjectAsync(bucketName, fileName, (responseObj) => {

            if (responseObj.Exception != null) {
                Debug.LogWarning("s3 exception: " + responseObj.Exception.Message);
                return;
            }
            var response = responseObj.Response;
            var reader = new StreamReader(response.ResponseStream, Encoding.Unicode);
            var file = reader.ReadToEnd();
            responseDelegate(file);
        });
    }

    public void WriteFile(string fileName,
                          string contents,
                          WriteFileResponseDelegate responseDelegate) {
        var bytes = Encoding.Unicode.GetBytes(contents);
        var stream = new MemoryStream(bytes);
        var request = new PutObjectRequest {
            InputStream = stream,
            BucketName = bucketName,
            Key = fileName,
            CannedACL = S3CannedACL.Private
        };
        client.PutObjectAsync(request, (responseObj) => {
            if (responseObj.Exception != null) {
                if (responseObj.Exception is Amazon.Runtime.Internal.HttpErrorResponseException) {
                    var exception = (Amazon.Runtime.Internal.HttpErrorResponseException)responseObj.Exception;
                    Debug.LogWarning("exception.ResponseCode: " + exception.Response.StatusCode);
                    Debug.LogWarning("exception.Response: " + exception.Response.ResponseBody.OpenResponse());
                }
                Debug.LogWarning("s3 exception: " + responseObj.Exception.Message +
                                 "\nstacktrace: " + responseObj.Exception.StackTrace +
                                 "\nhelplink: " + responseObj.Exception.HelpLink +
                                 "\ntoString" + responseObj.Exception.ToString());
                responseDelegate(false);
                return;
            }
            responseDelegate(true);
        });
    }


    public void DeleteFile(string fileName) {
        var request = new DeleteObjectRequest() {
            BucketName = bucketName,
            Key = fileName
        };
        Debug.Log("S3Client - deleting: " + fileName);
        client.DeleteObjectAsync(request, (responseObj) => {
            if (responseObj.Exception != null) {
                Debug.LogWarning(
                    "s3 exception: " + responseObj.Exception.Message +
                    "\nstacktrace: " + responseObj.Exception.StackTrace +
                    "\nhelplink: " + responseObj.Exception.HelpLink +
                    "\ntoString" + responseObj.Exception.ToString());
                return;
            }
            Debug.Log("S3Client - deleted file successfully: " + fileName);
        });
    }
}
