﻿using System;
using Newtonsoft.Json;

public static class JSONer {
    static readonly JsonSerializerSettings jsonSettings =
        new JsonSerializerSettings {
            TypeNameHandling = TypeNameHandling.All,
            Formatting = Formatting.Indented
        };

    public static T Deserialize<T>(string json) {
        return JsonConvert.DeserializeObject<T>(json, jsonSettings);
    }

    public static string Serialize(object o) {
        return JsonConvert.SerializeObject(o, jsonSettings);
    }
}
