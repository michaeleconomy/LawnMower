﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationPoint : IComparable {
    public Vector2 position;
    public List<Vector2> path;
    public float score;

    int IComparable.CompareTo(object obj) {
        var otherPoint = (NavigationPoint)obj;
        return score.CompareTo(otherPoint.score);
    }

    override public string ToString() {
        return "{position: " + position + ", score: " + score + "}";
    }
}
