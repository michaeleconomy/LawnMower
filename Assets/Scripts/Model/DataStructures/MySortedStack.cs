﻿using System;
using System.Collections;
using System.Collections.Generic;

public class MySortedStack<T> where T : class, IComparable
{
    private readonly List<T> list = new List<T>();

    public void Add(T t)
    {
        var index = 0;
        var maxIndex = list.Count;
        while (index != maxIndex)
        {
            var midPointIndex = (index + maxIndex) / 2;
            var midPointT = list[midPointIndex];
            var comparison = t.CompareTo(midPointT);
            if (comparison > 0)
            {
                index = midPointIndex + 1;
            }
            else if (comparison == 0)
            {
                index = midPointIndex + 1;
                break;
            }
            else
            {
                maxIndex = midPointIndex;
            }
        }
        list.Insert(index, t);
    }

    public T Pop()
    {
        if (list.Count == 0)
        {
            return null;
        }
        T t = list[0];
        list.RemoveAt(0);
        return t;
    }
}
