﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPlacer {
    public static int layerMask =
        LayerMask.GetMask("Default", "players", "enemies");
    private static float testColliderSize = 0.9f;
    private static Vector2 testCollider = 
        new Vector2(testColliderSize, testColliderSize);

    public static void PlaceAround(Vector3 position) {
        var players = TurnManager.instance.Players();
        foreach (var player in players) {
            var space = GetClearSpace(position);
            var body = player.GetComponent<Rigidbody2D>();
            body.position = space;
        }
    }

    private static Vector3 GetClearSpace(Vector3 position) {
        var roundedPosition = position.RoundPosition();
        var startX = roundedPosition.x;
        var startY = roundedPosition.y;
        for (var i = 0; i < 10; i++) {
            var boxWidth = 2 * i + 1;
            var boxArea = boxWidth * boxWidth;
            for (var j = 0; j < boxArea; j++) {
                var xOffset = j % boxWidth;
                var yOffset = j / boxWidth;
                var space = new Vector3(startX + xOffset,
                                        startY + yOffset,
                                        0);
                if (SpaceClear(space)) {
                    return space;
                }
                if (xOffset == 0 && yOffset > 0 && yOffset < (boxWidth - 1)) {
                    j += boxWidth - 2;
                }
            }

            startX -= 1;
            startY -= 1;
        }
        throw new Exception("clear space could not be found");
    }

    private static bool SpaceClear(Vector3 space) {
        if (!PlayerMapManager.instance.HasGround(Vector3Int.FloorToInt(space))) {
            //Debug.Log("space not clear, no ground" + space);
            return false;
        }
        var collider = Physics2D.OverlapBox(space, testCollider, 0f, layerMask);
        //if (collider != null) {
        //    Debug.Log("space not clear, collider: " + collider.name +
        //              " space: "+ space);
        //}
        return collider == null;
    }
}
