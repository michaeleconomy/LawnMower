﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : GameEntity {
    public Gender gender;

    public int strength = 9;
    public int dexterity = 9;
    public int constitution = 9;
    public int intelligence = 9;
    public int wisdom = 9;
    public int charisma = 9;

    public List<Equipment> startingEquipment = new List<Equipment>();
    public CharacterEquipment equipment = new CharacterEquipment();

    //TODO - replace this
    public int sightRange = 6;

    //TODO replace this
    public int level = 1;

    //TODO only account for ground movement right now (not fly/swim)
    //TODO should this be derived?
    public int moveSpeed = 6;
    public CharacterActions actions = new CharacterActions();

    public bool dead;

    public readonly List<string> proficiencies = new List<string>();

    public CharacterMovement movement;


    private readonly int deadAnimationParam = Animator.StringToHash("dead");
    private readonly int damagedAnimationParam = Animator.StringToHash("damaged");

    protected virtual void Reset() {
        isContainer = true;
    }

    protected override void Awake() {
        actions.character = this;
        movement = GetComponent<CharacterMovement>();
        base.Awake();
    }

    public override void RefreshSubItems() {
        RemoveAllEquipment();
        base.RefreshSubItems();
        foreach (var prefab in startingEquipment) {
            var item = Instantiate(prefab);
            item.name = prefab.name;
            Add(item);
            Equip(item);
        }
    }

    public bool HasBonusActions() {
        //TODO - implement bonus actions
        return false;
    }

    public virtual void ActionsUpdated() {
        //nothing - subclasses care
    }

    public int ProficiencyBonus() {
        return ((level - 1) / 4) + 2;
    }

    public int StrengthModifier(bool proficient = false) {
        return Modifier(strength, proficient);
    }

    public int DexterityModifier(bool proficient = false) {
        return Modifier(dexterity, proficient);
    }

    public int ConstitutionModifier(bool proficient = false) {
        return Modifier(constitution, proficient);
    }

    public int IntelligenceModifier(bool proficient = false) {
        return Modifier(intelligence, proficient);
    }
    public int WisdomModifier(bool proficient = false) {
        return Modifier(wisdom, proficient);
    }

    public int CharismaModifier(bool proficient = false) {
        return Modifier(charisma, proficient);
    }

    private int Modifier(int attribute, bool proficient = false) {
        var mod = (attribute - 10) / 2;
        if (proficient) {
            mod += ProficiencyBonus();
        }
        return mod;
    }

    public Initiative RollInitative(int roll = -1) {
        return new Initiative
        {
            roll = roll == -1 ? Dice.Roll(1, 20) : roll,
            modifier = DexterityModifier(),
            character = this
        };
    }

    public override int ArmorClass() {
        //TODO
        return 10;
    }

    public Attack DefaultAttack() {
        var sortedAttacks =
            Attacks().OrderByDescending((arg1) => arg1.AverageDamage()).ToList();
        return sortedAttacks[0];
    }

    public virtual List<Attack> Attacks() {
        var attacks = new List<Attack>();
        foreach (var item in equipment.hands) {
            if (item is Weapon) {
                var weapon = (Weapon)item;
                if (weapon.Ranged()) {
                    attacks.Add(new WeaponAttack {
                        attacker = this,
                        weapon = weapon,
                        attackType = AttackType.Ranged
                    });
                }
                if (weapon.Melee()) {
                    attacks.Add(new WeaponAttack {
                        attacker = this,
                        weapon = weapon,
                        attackType = AttackType.Melee
                    });
                }
            }
        }
        attacks.AddRange(BaseAttacks());
        return attacks;
    }

    public override string DisplayName() {
        if (DialogVariables.instance.Get("met" + name)) {
            return name.Replace("_", " ");
        }
        return GenericName();
    }


    protected override string GenericName() {
        if (!string.IsNullOrEmpty(genericName)) {
            return genericName;
        }
        switch (gender) {
            case Gender.FEMALE:
                return "Woman";
            case Gender.MALE:
                return "Man";
        }
        return "Stranger";
    }

    private List<Attack> BaseAttacks() {
        return new List<Attack> {
            new UnarmedAttack {
                attacker = this
            }
        };
    }

    public override void ApplyDamage(TrueDamage damage) {
        base.ApplyDamage(damage);
        if (damage.Sum() > 0) {
            PlayDamageAnimation();
        }

        if (hp <= 0 && !dead) {
            dead = true;
            OnDeath();
        }
    }

    protected void PlayDamageAnimation() {
        var animator = GetComponent<Animator>();
        if (animator != null && gameObject.activeSelf) {
            animator.SetTrigger(damagedAnimationParam);
        }
    }

    public bool DoingNonDefaultAnimation() {
        var animator = GetComponent<Animator>();
        //var currentState = animator.GetCurrentAnimatorStateInfo(0);
        var nextState = animator.GetNextAnimatorStateInfo(0);
        return !nextState.IsName("Idle");
    }

    public override int ApplyHealing(int amount) {
        if (dead) {
            return 0;
        }
        var total = base.ApplyHealing(amount);
        return total;
    }

    protected void UpdateAnimationDead() {
        var animator = GetComponent<Animator>();
        if (animator != null && gameObject.activeSelf) {
            animator.SetBool(deadAnimationParam, ShowDead());
            animator.ResetTrigger(damagedAnimationParam);
        }
    }

    protected virtual bool ShowDead() {
        return dead;
    }

    public void Unequip(Equipment item) {
        if (!contents.Contains(item)) {
            throw new Exception("can't unequip " + item.name + ", it's not in inventory");
        }
        switch (item.slot) {
            case Equipment.Slot.belt:
                if (equipment.belt == item) {
                    equipment.belt = null;
                }
                else {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.armor:
                if (equipment.armor == item) {
                    equipment.armor = null;
                }
                else {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.bracelets:
                if (!equipment.bracelets.Remove(item)) {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.gloves:
                if (equipment.gloves == item) {
                    equipment.gloves = null;
                }
                else {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.hands:
                if (!equipment.hands.Remove(item)) {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.helm:
                if (equipment.helm == item) {
                    equipment.helm = null;
                }
                else {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.necklace:
                if (!equipment.necklaces.Remove(item)) {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.rings:
                if (!equipment.rings.Remove(item)) {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            case Equipment.Slot.shoes:
                if (equipment.shoes == item) {
                    equipment.shoes = null;
                }
                else {
                    throw new Exception(item.name + " was not equipped!");
                }
                break;
            default:
                throw new Exception("unhandled equipment slot: " + item.slot);
        }
        Unwear(item);
    }

    public void Equip(Equipment item) {
        if (!contents.Contains(item)) {
            throw new Exception("can't equip " + item.name + ", it's not in inventory");
        }
        switch (item.slot) {
            case Equipment.Slot.belt:
                equipment.belt = item;
                break;
            case Equipment.Slot.armor:
                equipment.armor = item;
                break;
            case Equipment.Slot.bracelets:
                equipment.bracelets.Add(item);
                break;
            case Equipment.Slot.gloves:
                equipment.gloves = item;
                break;
            case Equipment.Slot.hands:
                equipment.hands.Add(item);
                break;
            case Equipment.Slot.helm:
                equipment.helm = item;
                break;
            case Equipment.Slot.necklace:
                equipment.necklaces.Add(item);
                break;
            case Equipment.Slot.rings:
                equipment.rings.Add(item);
                break;
            case Equipment.Slot.shoes:
                equipment.shoes = item;
                break;
            default:
                throw new Exception("unhandled equipment slot: " + item.slot);
        }

        Wear(item);
    }

    private void Wear(Equipment item) {
        var equipmentNode = GetEquipNode(item);
        if (equipmentNode == null) {
            return;
        }

        item.transform.SetParent(equipmentNode.transform);
        item.transform.localPosition = item.attachLocation;
        var equipmentSprite = item.GetComponent<SpriteRenderer>();
        equipmentSprite.sortingOrder = equipmentNode.sortingOrder + item.RenderOrder();
        equipmentSprite.sortingLayerID = equipmentNode.sortingLayerID;
        equipmentSprite.enabled = true;
    }

    private void Unwear(Equipment item) {
        var equipmentSprite = item.GetComponent<SpriteRenderer>();
        equipmentSprite.enabled = false;
        equipmentSprite.sortingOrder = 0;
        equipmentSprite.sortingLayerName = "items";
        item.transform.SetParent(contentsTransform);
    }

    private SpriteRenderer GetEquipNode(Equipment item) {
        if (item.AttachNodeName() == null) {
            return null;
        }
        return this.FindChildByName<SpriteRenderer>(item.AttachNodeName());
    }

    //NOTE: will delete stuff from inventory also
    private void RemoveAllEquipment() {
        foreach (var item in GetComponentsInChildren<Equipment>()) {
            contents.Remove(item);
            Destroy(item.gameObject);
        }
    }

    public abstract IEnumerator TakeTurn();

    protected virtual void OnDeath() {
        UpdateAnimationDead();
        SetLayer();
        LogManager.Log(DisplayName() + " has been killed.");
        ExperienceManager.instance.CharacterKilled(this);
    }

    public void Revive(int healing) {
        dead = false;
        OnRevive();
        ApplyHealing(healing);
    }

    protected virtual void OnRevive() {
        UpdateAnimationDead();
        SetLayer();
        LogManager.Log(name + " has been revived.");
    }

    private void SetLayer() {
        SetRenderLayer();
        if (dead) {
            gameObject.layer = LayerMask.NameToLayer("items");
            return;
        }

        if (this is NonPlayerCharacter) {
            var npc = (NonPlayerCharacter)this;
            if (!npc.friendly) {
                gameObject.layer = LayerMask.NameToLayer("enemies");
            }
            else {
                gameObject.layer = LayerMask.NameToLayer("Default");
            }
        }
        else {
            gameObject.layer = LayerMask.NameToLayer("players");
        }
    }
    private void SetRenderLayer() {
        var spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (var spriteRenderer in spriteRenderers) {
            spriteRenderer.sortingLayerName = dead ? "items" : "characters";
        }
    }

     public void OnLoad() {
        SetLayer();

        var animator = GetComponent<Animator>();
        if (animator != null && gameObject.activeSelf && ShowDead()) {
            animator.SetBool(deadAnimationParam, true);
            animator.PlayInFixedTime("Death", -1, 1f);
        }
    }

    public bool Proficient(string thing) {
        return proficiencies.Contains(thing);
    }

    public bool Proficient(Weapon weapon) {
        return Proficient(weapon.type.ToString()) ||
            Proficient(weapon.ProficencyName());
    }
}
