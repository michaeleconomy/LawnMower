﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterEquipment {
    public Equipment armor;
    public Equipment helm;
    public Equipment gloves;
    public Equipment shoes;
    public Equipment belt;

    public List<Equipment> hands = new List<Equipment>();
    public List<Equipment> rings = new List<Equipment>();
    public List<Equipment> bracelets = new List<Equipment>();
    public List<Equipment> necklaces = new List<Equipment>();
}
