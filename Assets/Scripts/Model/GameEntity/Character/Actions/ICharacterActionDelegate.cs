﻿using System;
public interface ICharacterActionDelegate {
    void TurnEnd(Character character);
    void AvailibleActionsChanged(Character character);
}
