﻿using System;
using System.Collections.Generic;

public class WeaponAttack : Attack {
    public Weapon weapon;
    public AttackType attackType;

    public override int AttackModifier() {
        var proficient = attacker.Proficient(weapon);
        return Modifier(proficient);
    }

    public override Damage Damage(bool critical) {
        return ModifiedDamage(weapon.BaseDamage(critical));
    }

    public Damage MinDamage() {
        return ModifiedDamage(weapon.MinDamage());
    }

    public Damage MaxDamage() {
        return ModifiedDamage(weapon.MaxDamage());
    }

    private Damage ModifiedDamage(Damage baseDamage) {
        if (baseDamage.Count == 0) {
            throw new Exception("expected there to be damage to modify");
        }
        var modifiedDamage = new Damage(baseDamage);

        var damageAmount = baseDamage[0].amount + Modifier(false);
        if (damageAmount < 0) {
            damageAmount = 0;
        }
        modifiedDamage[0] = new Damage.Part {
            amount = damageAmount,
            type = baseDamage[0].type
        };

        return modifiedDamage;
    }

    public override float Range() {
        return weapon.Range();
    }

    private int Modifier(bool includeProficiency) {
        switch (weapon.AbilityModifier(attackType)) {
            case Weapon.ModifierType.Dexterity:
                return attacker.DexterityModifier(includeProficiency);
            case Weapon.ModifierType.Strength:
                return attacker.StrengthModifier(includeProficiency);

        }
        return Math.Max(attacker.DexterityModifier(includeProficiency),
                        attacker.StrengthModifier(includeProficiency));
    }

    public override float AverageDamage() {
        return weapon.AverageDamage() + Modifier(false);
    }
}
