﻿using System;
using System.Collections.Generic;

public class AttackResult {
    public Attack attack;
    public GameEntity target;
    public bool hit;
    public bool critical;
    public Damage damage;
    public TrueDamage trueDamage;
}
