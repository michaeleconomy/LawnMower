﻿using System;
using Newtonsoft.Json;

[Serializable]
public class CharacterActions {

    [NonSerialized]
    public Character character;

    public bool doneWithTurn;
    public bool hasStandardAction;
    public bool hasBonusAction;
    public bool hasReaction;
    public int movesLeft;

    private void CheckDoneWithTurn() {
        if (doneWithTurn) {
            return;
        }
        if (movesLeft > 0) {
            return;
        }
        if (hasStandardAction) {
            return;
        }
        if (hasBonusAction && character.HasBonusActions()) {
            return;
        }
        doneWithTurn = true;
    }


    public void EndTurn() {
        hasBonusAction = false;
        hasStandardAction = false;
        movesLeft = 0;
        doneWithTurn = true;
    }

    public bool DoneWithTurn() {
        return doneWithTurn;
    }

    public bool HasStandardAction() {
        return hasStandardAction;
    }

    public bool HasBonusAction() {
        return hasBonusAction;
    }

    public int MovesLeft() {
        return movesLeft;
    }

    //NOTE: only to be called by the turn manager!
    public void RefreshActions() {
        doneWithTurn = false;
        movesLeft = character.moveSpeed;
        hasStandardAction = true;
        hasBonusAction = true;
        hasReaction = true;
        character.ActionsUpdated();
    }

    public void TakeRunAction() {
        if (!hasStandardAction) {
            throw new Exception("cannot use standard action as you do not have it left");
        }
        hasStandardAction = false;
        movesLeft += character.moveSpeed;
        character.ActionsUpdated();
    }

    public void UseStandardAction() {
        if (!hasStandardAction) {
            throw new Exception("cannot use standard action as you do not have it left");
        }
        hasStandardAction = false;
        CheckDoneWithTurn();
        character.ActionsUpdated();
    }

    public void UseBonusAction() {
        if (!hasBonusAction) {
            throw new Exception("cannot use bonus action as you do not have it left");
        }
        hasBonusAction = false;
        CheckDoneWithTurn();
        character.ActionsUpdated();
    }

    public void UseReaction() {
        if (!hasReaction) {
            throw new Exception("cannot use reaction as you do not have it left");
        }
        hasReaction = false;
        character.ActionsUpdated();
    }

    public void UseMove(int count = 1) {
        if (movesLeft < count) {
            throw new Exception("not enough moves left! (moves left: " + movesLeft + ", count: " + count + ")");
        }
        movesLeft -= count;
        CheckDoneWithTurn();
        character.ActionsUpdated();
    }
}
