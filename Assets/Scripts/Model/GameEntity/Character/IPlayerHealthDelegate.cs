﻿using System;
public interface IPlayerHealthDelegate {
    void PlayerHealthChanged(PlayerCharacter character);
}
