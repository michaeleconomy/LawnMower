﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidBody : MonoBehaviour {

    public int hairNumber;
    public int hairBackNumber;
    public Color hairColor;

    public Color skinColor;

    public int shirtNumber;
    public Color shirtColor;

    public int pantsNumber;
    public Color pantsColor;

    public int facialHairNumber;
    public Color shoeColor;

    public int headAccessoryNumber;
    public Color headAccessoryColor;
    public int torsoAccessoryNumber;
    public Color torsoAccessoryColor;

    public List<Sprite> hairSprites;
    public List<Sprite> facialHairSprites;
    public List<Sprite> hairBackSprites;
    public List<Sprite> shirtSprites;
    public List<Sprite> pantsSprites;
    public List<Sprite> headAccessorySprites;
    public List<Sprite> torsoAccessorySprites;

    public void RefreshAppearance() {
        SetSprite(hairNumber, hairSprites, "hair");
        SetSprite(hairBackNumber, hairBackSprites, "hairBack");
        SetSprite(facialHairNumber, facialHairSprites, "facialHair");
        SetColor(hairColor,
                 "hair",
                 "hairBack",
                 "leftEyeBrow",
                 "rightEyeBrow",
                 "facialHair");

        SetSprite(shirtNumber, shirtSprites, "shirt");
        SetColor(shirtColor, "shirt");

        SetSprite(shirtNumber, shirtSprites, "shirt");
        SetColor(shirtColor, "shirt");

        SetSprite(pantsNumber, pantsSprites, "pants");
        SetColor(pantsColor, "pants");

        SetColor(shoeColor, "leftFoot", "rightFoot", "belt");

        SetColor(skinColor, "head", "leftHand", "rightHand");

        SetSprite(headAccessoryNumber, headAccessorySprites, "headAccessory");
        SetColor(headAccessoryColor, "headAccessory");

        SetSprite(torsoAccessoryNumber, torsoAccessorySprites, "torsoAccessory");
        SetColor(torsoAccessoryColor, "torsoAccessory");
    }

    private void SetSprite(int spriteId,
                           List<Sprite> sprites,
                           string nodeName) {
        var spriteRenderer = this.FindChildByName<SpriteRenderer>(nodeName);
        if (spriteRenderer == null) {
            return;
        }
        Sprite sprite = null;
        if (spriteId < sprites.Count && spriteId >= 0) {
            sprite = sprites[spriteId];
        }
        spriteRenderer.sprite = sprite;
    }
    private void SetColor(Color color, params string[] spriteRenderers) {
        foreach (var nodeName in spriteRenderers) {
            var spriteRenderer = this.FindChildByName<SpriteRenderer>(nodeName);
            if (spriteRenderer == null) {
                continue;
            }
            spriteRenderer.color = color;
        }
    }

    private void Toggle(bool isEnabled, string nodeName) {
        var spriteRenderer = this.FindChildByName<SpriteRenderer>(nodeName);
        if (spriteRenderer == null) {
            return;
        }
        spriteRenderer.enabled = isEnabled;
    }
}
