﻿using UnityEngine;

public class CharacterDetector : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D otherCollider) {
        if (World.inEditMode) {
            return;
        }
        var otherCharacter = otherCollider.gameObject.GetComponent<Character>();
        if (otherCharacter != null && otherCharacter.active) {
            TurnManager.instance.AddCharacter(otherCharacter);
        }
    }
}
