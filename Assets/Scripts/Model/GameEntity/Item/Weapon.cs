﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Equipment {
    public int numDamageDice = 1;
    public int damageDiceSize = 6;
    public DamageType damageType = DamageType.Slashing;

    public Type type;

    public int range;
    public int longRange;

    public bool finesse;
    public bool heavy;
#if UNITY_EDITOR
    new public bool light;
#else
     public bool light;
#endif
    public bool loading;
    public bool reach;
    public bool thrown;
    public bool twoHanded;
    public bool versatile;
    public int versatileDiceSize;

    public string proficencyName;

    public enum ModifierType {
        Strength,
        Dexterity,
        Either
    }

    public enum Type {
        simpleMelee,
        simpleRanged,
        martialMelee,
        martialRanged
    }

    public void Reset() {
        slot = Slot.hands;
    }

    public float Range() {
        if (longRange > 0) {
            return longRange / 5f;
        }
        if (reach) {
            return 2f;
        }
        return 1f;
    }

    //this doesn't incldue the weilder's modifiers (Strength/dex)
    public Damage BaseDamage(bool critical) {
        var numDice = numDamageDice;
        if (critical) {
            numDamageDice *= 2;
        }
        var damage = new Damage.Part {
            type = damageType,
            amount = Dice.Roll(numDice, damageDiceSize)
        };
        return new Damage{
            damage
        };
    }

    public Damage MaxDamage() {
        var damage = new Damage.Part {
            type = damageType,
            amount = numDamageDice * damageDiceSize
        };
        return new Damage{
            damage
        };
    }

    public Damage MinDamage() {
        var damage = new Damage.Part {
            type = damageType,
            amount = numDamageDice * 1
        };
        return new Damage{
            damage
        };
    }


    public float AverageDamage() {
        return Dice.AverageRoll(numDamageDice, damageDiceSize);
    }

    public ModifierType AbilityModifier(AttackType attackType) {
        if (finesse) {
            return ModifierType.Either;
        }
        if (attackType == AttackType.Melee) {
            return ModifierType.Strength;
        }
        //is ranged
        if (thrown) {
            return ModifierType.Strength;
        }
        return ModifierType.Dexterity;
    }

    public bool Ranged() {
        return longRange > 0;
    }

    public bool Melee() {
        return thrown || !Ranged();
    }

    public string ProficencyName() {
        if (proficencyName != null) {
            return proficencyName;
        }
        return name;
    }
}
