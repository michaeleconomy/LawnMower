﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : GameEntity {
    public float value; //in gp
    public bool stacks = false; // will this item stack up in the characters inventory
    public int quantity = 1;
    public int armorClass = 11;
    public bool questItem;

    public int Quantity {
        get { return quantity; }
        set {
            if (!stacks && value != 1) {
                throw new Exception(name + " does not stack");
            }
            if (value <= 0) {
                throw new Exception("value cannot be zero or less");
            }
            quantity = value;
            QuantityUpdated();
        }
    }

    //in ounces
    public override float Weight() {
        return individualWeight * Quantity + ContentsWeight();
    }

    protected virtual void QuantityUpdated() { } //needed by children

    public override int ArmorClass() {
        return armorClass;
    }

    public void Hide() {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        var col = GetComponent<Collider2D>();
        col.enabled = false;
    }

    public void Show() {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = true;
        var col = GetComponent<Collider2D>();
        col.enabled = true;
    }

    public override SavedPrefab Save() {
        return new SavedItem(this);
    }
}
