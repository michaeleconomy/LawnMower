﻿using System;
using UnityEngine;

public class Equipment : Item {
    public Slot slot;
    public Vector2 attachLocation;
    public enum Slot {
        armor,
        gloves,
        rings,
        helm,
        hands,
        shoes,
        belt,
        bracelets,
        necklace
    }

    public string AttachNodeName() {
        switch (slot) {
            case Equipment.Slot.armor:
                return "torso";
            case Equipment.Slot.belt:
                return "torso";
            case Equipment.Slot.hands:
                return "rightHand";
            case Equipment.Slot.helm:
                return "head";
            case Equipment.Slot.necklace:
                return "torso";
        }
        return null;
    }

    public int RenderOrder() {
        switch (slot) {
            case Equipment.Slot.armor:
                return 4;
            case Equipment.Slot.belt:
                return 5;
            case Equipment.Slot.hands:
                return -1;
            case Equipment.Slot.helm:
                return 1;
            case Equipment.Slot.necklace:
                return 6;
        }
        return 0;
    }
}
