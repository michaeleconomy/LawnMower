﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GameEntity : ManagedPrefab {
    public int hp = 10;
    public int hpMax = 10;
    public bool isContainer = false;
    public List<Item> contents = new List<Item>();
    public List<Item> startingContents = new List<Item>();
    public Transform contentsTransform;

    // in ounces (of quantity of 1)
    public float individualWeight;


    protected override void Awake() {
        base.Awake();
        if (isContainer) {
            var contentsContainer = new GameObject {
                name = "contents"
            };
            contentsTransform = contentsContainer.transform;
            contentsTransform.SetParent(this.transform);
            RefreshSubItems();
        }
    }

    public virtual int ArmorClass() {
        return 10;
    }

    public virtual void RefreshSubItems() {
        contents.Clear();
        contentsTransform.DeleteChildren();
        foreach (var itemPrefab in startingContents) {
            var item = Instantiate(itemPrefab);
            item.name = itemPrefab.name;
            Add(item);
        }
    }

    public void Add(Item item) {
        if (!isContainer) {
            throw new Exception(name + " is not a container!");
        }
        if (item.questItem && this is PlayerCharacter) {
            QuestManager.instance.CompleteObjective("get" + item.name);
        }
        item.Hide();
        item.transform.SetParent(contentsTransform, false);
        contents.Add(item);
    }

    public void Remove(Item item) {
        if (!isContainer) {
            throw new Exception(name + " is not a container!");
        }
        if (contents.Remove(item)) {
            item.transform.position = transform.position;
            item.transform.SetParent(World.instance.transform);
            item.Show();
        }
    }

    public Item GetItemByName(string name) {
        foreach (var content in contents) {
            if (content.name == name) {
                return content;
            }
        }
        return null;
    }

    public virtual int ApplyHealing(int amount) {
        if (amount < 1) {
            throw new Exception("healing amount is less than 1: " + amount);
        }
        var adjustedAmount = amount;
        if ((hp + amount) > hpMax) {
            adjustedAmount = hpMax - hp;
        }


        hp += adjustedAmount;
        return adjustedAmount;
    }

    public virtual void ApplyDamage(TrueDamage damage) {
        GameEntityApplyDamage(damage);
    }

    public TrueDamage GetTrueDamage(Damage damage) {
        var trueDamage = new TrueDamage();
        foreach (var d in damage) {
            //TODO factor in resistances
            trueDamage.Add(d);
        }
        return trueDamage;
    }

    protected void GameEntityApplyDamage(TrueDamage damage) {
        hp -= damage.Sum();
    }


    //in ounces
    public virtual float Weight() {
        return individualWeight + ContentsWeight();
    }

    public float ContentsWeight() {
        if (!isContainer) {
            return 0f;
        }
        var total = 0f;
        foreach (var item in contents) {
            total += item.Weight();
        }
        return total;
    }


    public override SavedPrefab Save() {
        return new SavedGameEntity(this);
    }
}
