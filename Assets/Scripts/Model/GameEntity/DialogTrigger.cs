﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class DialogTrigger : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log(collision.name + " has triggered the trigger");
        StartCoroutine(HaltAndDoDialog());
    }

    private IEnumerator HaltAndDoDialog() {
        var managedPrefab = GetComponentInParent<ManagedPrefab>();
        if (managedPrefab == null) {
            Debug.LogWarning("trigger fired, but no managed prefab was found!");
            yield break;
        }
        var currentPlayer = TurnManager.instance.CurrentPlayer();
        if (currentPlayer != null) {
            yield return StartCoroutine(currentPlayer.movement.HaltAndWait());
        }

        DialogRunner.instance.StartDialog(managedPrefab);
        Destroy(managedPrefab.gameObject);
    }
}
