﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TriggerManager : MonoBehaviour {
    public static TriggerManager instance;

    public TriggerData triggerData;

    public readonly List<ReadiedTrigger> readiedTriggers =
        new List<ReadiedTrigger>();

    private void Awake() {
        instance = this;
    }

    public void CheckTriggers() {
        foreach (var trigger in readiedTriggers) {
            if (trigger.ShouldFire()) {
                readiedTriggers.Remove(trigger);
                DialogRunner.instance.StartDialog(
                    trigger.name, trigger.mapName != TriggerData.anywhere);
                return; //TODO only doing one at a time right now, should queue them up
            }
        }
    }

    public void ReadyTrigger(string triggerName, int relativeTime = 0) {
        var trigger = triggerData.triggers.First((t) => t.name == triggerName);
        if (trigger == null) {
            Debug.LogWarning("trigger could not be found: " + triggerName);
            return;
        }
        readiedTriggers.Add(new ReadiedTrigger {
            name = triggerName,
            mapName = trigger.mapName,
            time = TimeManager.instance.currentTime + relativeTime
        });
    }
}
