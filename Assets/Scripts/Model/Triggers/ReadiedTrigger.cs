﻿using System;
public class ReadiedTrigger : Trigger {
    public int time;

    public bool ShouldFire() {
        var mapMatch = mapName == TriggerData.anywhere ||
            mapName == PlayerMapManager.instance.currentMap;
        return mapMatch && time <= TimeManager.instance.currentTime;
    }
}
