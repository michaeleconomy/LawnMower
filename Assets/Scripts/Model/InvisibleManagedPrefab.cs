﻿using System;
using UnityEngine;

public class InvisibleManagedPrefab : ManagedPrefab {
    protected override void Awake() {
        base.Awake();
        GetComponent<SpriteRenderer>().enabled = World.inEditMode;
    }
}
