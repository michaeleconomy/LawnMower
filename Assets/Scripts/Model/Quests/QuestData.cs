﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class QuestData : ScriptableObject {
    public List<Quest> quests;
}
