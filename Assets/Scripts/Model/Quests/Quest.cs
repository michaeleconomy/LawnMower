﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Quest {
    public string id;
    public string title;
    public string description;
    public List<Objective> objectives;
    public int xp;

    [Serializable]
    public class Objective {
        public int order;
        public string key;
        public string description;

        public bool Completed() {
            return QuestManager.instance.ObjectiveCompleted(key);
        }
    }

    public bool Completed() {
        foreach (var objective in objectives) {
            if (!objective.Completed()) {
                return false;
            }
        }
        return true;
    }

    public int CurrentObjectiveOrder() {
        foreach (var objective in objectives) {
            if (!objective.Completed()) {
                return objective.order;
            }
        }
        Debug.LogWarning("no uncompleted objectives!");
        return -1;
    }
}
