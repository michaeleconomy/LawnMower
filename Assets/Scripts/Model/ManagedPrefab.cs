﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ManagedPrefab : MonoBehaviour {
    private static Regex validNameRegex = new Regex(@"^\w+$");

    public static bool ValidName(string name) {
        return validNameRegex.IsMatch(name);
    }

    [NonSerialized]
    public string id;

    public int prefabId;

    public bool active = true;
    public bool hasDialog = false;
    public bool disableCollider = false;
    public bool scalable = false;
    public bool freePlace = false;
    public string genericName;
    private GameObject inactiveMarker;

    public bool varyColor = false;
    private Color _color = Color.white;
    public Color Color {
        get { return _color; }
        set {
            if (!varyColor) {
                Debug.LogWarning("attempting to set color on " + name +
                                 " which doesn't allow color variants");
                return;
            }
            _color = value;
            var spriteRenderer = GetComponent<SpriteRenderer>();
            if (spriteRenderer == null) {
                Debug.LogWarning(name +
                                 " doesn't have a sprite renderer to color");
                return;
            }
            spriteRenderer.color = _color;
        }
    }

    public Color minColor = Color.black;
    public Color maxColor = Color.white;

    public void SetRandomColor() {
        float hueMin, hueMax, satMin, satMax, valMin, valMax;
        Color.RGBToHSV(minColor, out hueMin, out satMin, out valMin);
        Color.RGBToHSV(maxColor, out hueMax, out satMax, out valMax);
        Color = UnityEngine.Random.ColorHSV(hueMin, hueMax,
                                            satMin, satMax,
                                            valMin, valMax);
    }


    public bool randomSprite = false;
    public List<Sprite> randomSprites = new List<Sprite>();

    private int randomSpriteIndex;
    public int RandomSpriteIndex {
        get { return randomSpriteIndex; }
        set {
            if (!randomSprite) {
                Debug.LogWarning("attempting to set random sprite on " + name +
                                 " which doesn't allow randon sprites");
                return;
            }
            if (randomSprites == null) {
                Debug.LogWarning(name + " doesn't have randon sprites");
                return;
            }
            if (value < 0 || value >= randomSprites.Count) {
                Debug.LogWarning("value out of range: " + value);
                return;
            }
            var spriteRenderer = GetComponent<SpriteRenderer>();
            if (spriteRenderer == null) {
                Debug.LogWarning(name +
                                 " doesn't have a sprite renderer to put a random sprite in");
                return;
            }
            randomSpriteIndex = value;
            spriteRenderer.sprite = randomSprites[randomSpriteIndex];
        }
    }

    public void SetRandomSprite() {
        if (randomSprites == null || randomSprites.Empty()) {
            Debug.LogWarning(name + " doesn't have randon sprites");
            return;
        }
        RandomSpriteIndex = UnityEngine.Random.Range(0, randomSprites.Count);
    }

    protected virtual void Awake() {
        if (disableCollider && !World.inEditMode) {
            var col = GetComponent<BoxCollider2D>();
            col.enabled = false;
        }
    }

    public void SetActive(bool _active) {
        active = _active;
        RefreshActiveDisplay();
    }

    public void RefreshActiveDisplay() {
        gameObject.SetActive(!Application.isPlaying || World.inEditMode || active);
        SetMarker((!Application.isPlaying || World.inEditMode) && !active);
    }

    private void SetMarker(bool on) {
        if (inactiveMarker == null) {
            var markerComponent = GetComponentInChildren<InactiveMarker>();
            if (markerComponent != null) {
                inactiveMarker = markerComponent.gameObject;
            }
        }
        if (on) {
            if (inactiveMarker == null) {
                var inactiveMarkerPrefab = MapEditor.InactiveMarkerPrefab();
                if (inactiveMarkerPrefab == null) {
                    return;
                }
                inactiveMarker =
                    Instantiate(inactiveMarkerPrefab, transform, false);
            }
            inactiveMarker.SetActive(true);
            return;
        }

        if (inactiveMarker != null) {
            Destroy(inactiveMarker);
            inactiveMarker = null;
        }
    }

    public virtual bool HasDialog() {
        return hasDialog;
    }


    protected virtual string GenericName() {
        if (!string.IsNullOrEmpty(genericName)) {
            return genericName.Replace("_", " ");
        }
        return name.Replace("_", " ");
    }

    public virtual string DisplayName() {
        return GenericName();
    }

    public string Id() {
        if (string.IsNullOrEmpty(id)) {
            var millis = (long)EpochTime.Seconds();
            id = millis.ToString() + UnityEngine.Random.Range(0, int.MaxValue);
        }
        return id;
    }

    public void EnableOutline(Color color) {
        foreach (var outline in GetComponentsInChildren<OutlineObject>()) {
            outline.enabled = true;
            outline.outlineColor = color;
            outline.Regenerate();
            outline.Show();
        }
    }

    public void HideOutline() {
        foreach (var outline in GetComponentsInChildren<OutlineObject>()) {
            outline.Hide();
        }
    }


    public virtual SavedPrefab Save() {
        return new SavedPrefab(this);
    }

    public void Destroy() {
        Destroy(gameObject);
        DialogData.instance.Remove(EditMapManager.instance.currentMap, name);
    }
}
