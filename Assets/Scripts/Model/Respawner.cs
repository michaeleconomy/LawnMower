﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour {
    public static Respawner instance;

    public string lastGraveyardArea;
    public string lastGraveyardName;

    private void Awake() {
        instance = this;
    }

    public void DetectGraveyard() {
        var graveyard = World.instance.GetComponentInChildren<Graveyard>();
        if (graveyard == null) {
            return;
        }
        lastGraveyardName = graveyard.name;
        lastGraveyardArea = PlayerMapManager.instance.currentMap;
    }

    public void RespawnParty() {
        //TODO time passes
        //TODO heal enemies
        foreach (var player in TurnManager.instance.Players()) {
            RevivePlayer(player);
        }
        if (lastGraveyardArea == null) {
            Debug.LogError("no graveyards have ever been seen? this must be a bug!");
            return;
        }
        PlayerMapManager.instance.Travel(lastGraveyardArea, lastGraveyardName, 0);
        LogManager.Log("The party has awakened in a graveyard.  \n" +
                       "Their wounds are healed, and they are no longer dead.");
    }

    private void RevivePlayer(PlayerCharacter player) {
        player.Revive(1000000);
        //TODO remove all statuses
    }

}
