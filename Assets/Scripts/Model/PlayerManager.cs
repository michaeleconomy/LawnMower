﻿using System;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    public static PlayerManager instance;
    public PlayerCharacter male;
    public PlayerCharacter female;

    private void Awake() {
        instance = this;
    }

    public void ConvertToPc(NonPlayerCharacter npc) {
        PlayerCharacter prefab;
        switch (npc.gender) {
            case Gender.FEMALE:
                prefab = female;
                break;
            case Gender.MALE:
                prefab = male;
                break;
            default:
                Debug.LogWarning("don't know how to copy gender: " + npc.gender);
                prefab = female;
                break;
        }
        npc.gameObject.SetActive(false);
        var pc = Instantiate(prefab,
                             npc.transform.position,
                             npc.transform.rotation,
                             npc.transform.parent);
        pc.name = npc.name;
        pc.strength = npc.strength;
        pc.dexterity = npc.dexterity;
        pc.constitution = npc.constitution;
        pc.intelligence = npc.intelligence;
        pc.wisdom = npc.wisdom;
        pc.charisma = npc.charisma;
        pc.hp = npc.hp;
        pc.hpMax = npc.hpMax;
        pc.gender = npc.gender;
        pc.moveSpeed = npc.moveSpeed;
        pc.level = npc.level;
        var levelXP = PlayerCharacter.levelXp.First((l) => l.level == npc.level);
        pc.xp = levelXP == null ? 0 : levelXP.xp;
        pc.proficiencies.AddRange(npc.proficiencies);
        foreach (var item in npc.contents) {
            pc.Add(item);
            if (item is Equipment) {
                pc.Equip((Equipment)item);
            }
        }

        var oldBody = npc.GetComponent<HumanoidBody>();
        var newBody = pc.GetComponent<HumanoidBody>();

        new SavedHumanoidBody(oldBody).Restore(newBody);

        TurnManager.instance.RemoveCharacter(npc);
        TurnManager.instance.AddCharacter(pc);
        Destroy(npc.gameObject);
    }
}
