﻿using System;
public interface ICombatStatusDelegate {
    void CombatStarted();
    void CombatEnded();
}
