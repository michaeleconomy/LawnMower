﻿using System;
using System.Linq;
using System.Text;
using UnityEngine;

public class AttackDelegate : MonoBehaviour {
    public static AttackDelegate instance;

    private void Awake() {
        instance = this;
    }

    public void HandleAttack(AttackResult result) {
        var text = new StringBuilder(result.attack.attacker.DisplayName());
        if (result.hit) {
            var totalDamage = result.trueDamage.Sum();
            text.Append(" hit ");
            text.Append(result.target.DisplayName());
            text.Append(" for ");
            var damageParts =
                result.trueDamage.Select((d) => d.amount.ToString() + " " + d.type).ToList();
            text.Append(damageParts.ToSentence());
            text.Append(" damage");
            LogManager.Log(text.ToString());
            if (totalDamage > 0 && result.target is Character) {
                var targetCharacter = (Character)result.target;
                ExperienceManager.instance.DamageDone(result.attack.attacker, targetCharacter);
            }
            return;
        }
        text.Append(" missed " + result.target.DisplayName());
        LogManager.Log(text.ToString());
        return;
    }
}
