﻿using System;
using UnityEngine;
using UnityEngine.Sprites;

public class SpriteMunger : MonoBehaviour {
    private static SpriteMunger instance;
    public Camera mungeCamera;

    private void Awake() {
        instance = this;
    }

    public static Sprite Munge(MonoBehaviour go) {
        return instance.MungeObject(go.gameObject);
    }


    public static Sprite MungeHead(MonoBehaviour go) {
        var head = go.transform.Find("head");
        if (head != null) {
            return instance.MungeObject(head.gameObject);
        }
        return instance.MungeObject(go.gameObject);
    }

    private Sprite MungeObject(GameObject go) {
        Move(); //so we don't recapture deleted objects
        var copy = Instantiate(go, CenteredPosition(), Quaternion.identity);
        var outline = copy.transform.Find("Outline");
        if (outline != null) {
            var oldPosition = outline.position;
            outline.position = new Vector3(oldPosition.x, oldPosition.y + 100, 0);
        }
        ZoomCamera(copy);
        var renderTexture = mungeCamera.targetTexture;
        var currentRT = RenderTexture.active;
        RenderTexture.active = renderTexture;
        mungeCamera.Render();

        // Make a new texture and read the active Render Texture into it.
        Texture2D image = new Texture2D(renderTexture.width, renderTexture.height);
        var textureRect = new Rect(0, 0, renderTexture.width, renderTexture.height);
        image.ReadPixels(textureRect, 0, 0);
        image.Apply();
        RenderTexture.active = currentRT;

        Destroy(copy);
        var sprite = Sprite.Create(image, textureRect, Vector2.zero);
        var padding = DataUtility.GetPadding(sprite);

        Texture2D image2 = new Texture2D(
            Mathf.FloorToInt(renderTexture.width - (padding.x + padding.z)),
            Mathf.FloorToInt(renderTexture.height - (padding.y + padding.w)));

        image2.SetPixels(image.GetPixels(
            Mathf.FloorToInt(padding.x),
            Mathf.FloorToInt(padding.y),
            image2.width,
            image2.height
            ));
        image2.Apply();
        return Sprite.Create(image2, new Rect(0,0, image2.width, image2.height), Vector2.zero);
    }

    private void ZoomCamera(GameObject copy) {
        float maxDimension;
        var boxCollider = copy.GetComponent<BoxCollider2D>();
        if (boxCollider != null) {
            var bounds = boxCollider.bounds;
            maxDimension = Mathf.Max(bounds.size.x, bounds.size.y);
        }
        else {
            var spriteRenderer = copy.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null) {
                var bounds = spriteRenderer.bounds;
                maxDimension = Mathf.Max(bounds.size.x, bounds.size.y);
            }
            else {
                maxDimension = 1f;
            }
        }

        mungeCamera.orthographicSize = Mathf.CeilToInt(maxDimension * 2f);
    }

    private Vector3 CenteredPosition() {
        var cameraPos = mungeCamera.transform.position;
        return new Vector3(cameraPos.x, cameraPos.y, 0);
    }


    private void Move() {
        var oldPosition = transform.position;
        transform.position = new Vector3(oldPosition.x + 5, oldPosition.y, 0);
    }
}
