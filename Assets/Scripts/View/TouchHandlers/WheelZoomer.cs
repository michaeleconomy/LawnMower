﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class WheelZoomer : MonoBehaviour, IScrollHandler {

    void IScrollHandler.OnScroll(PointerEventData eventData) {
        var scrollChange = eventData.scrollDelta.y;
        var zoom = (scrollChange / 50) + 1;
        CameraController.ZoomRelative(zoom);
    }
}
