﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenPanner : MonoBehaviour,
        IDragHandler, IBeginDragHandler, IEndDragHandler {

    private Vector2 startedDragAtScreen;
    private Vector3 cameraStartingPlace;

    void IDragHandler.OnDrag(PointerEventData eventData) {
        if (DisablePanning(eventData.button)) {
            return;
        }
        DoPan();
    }

    private bool DisablePanning(PointerEventData.InputButton button) {
        if (!World.inEditMode) {
            return false;
        }
        if (Input.touchSupported) {
            return Input.touchCount <= 1;
        }
        return button == PointerEventData.InputButton.Left;
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        if (DisablePanning(eventData.button)) {
            return;
        }
        StartPan();
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        EndPan();
    }

    private Vector2 GetPrimaryPosition() {
        if (!Input.touchSupported) {
            return Input.mousePosition;
        }
        return Input.touches[0].position;
    }

    private void StartPan() {
        var position = GetPrimaryPosition();
        cameraStartingPlace = Camera.main.transform.position;
        startedDragAtScreen = position;
    }

    private void DoPan() {
        var position = GetPrimaryPosition();
        var screenDragLocation = ConstrainToWindow(position);
        var dragDifference = startedDragAtScreen - screenDragLocation;
        var cameraOffset =
            Camera.main.orthographicSize * 2f * dragDifference /
                  Camera.main.pixelHeight;

        var newCameraPosition =
            new Vector2(cameraStartingPlace.x + cameraOffset.x,
                        cameraStartingPlace.y + cameraOffset.y);
        CameraController.MoveTo(newCameraPosition);
    }

    private Vector2 ConstrainToWindow(Vector2 position) {
        var screenPosition = Vector2.Min(position, Camera.main.pixelRect.size);
        return Vector2.Max(Vector2.zero, screenPosition);
    }

    private void EndPan() {
        if (!World.inEditMode) {
            CameraController.Center();
        }
    }

}
