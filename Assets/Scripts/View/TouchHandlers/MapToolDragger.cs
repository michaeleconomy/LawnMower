﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MapToolDragger : MonoBehaviour,
        IDragHandler, IBeginDragHandler, IEndDragHandler {


    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }
        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }

        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.StartToolDrag(worldClickLocation, null);
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }
        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }


        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.ToolDrag(worldClickLocation, null);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        if (!World.inEditMode) {
            return;
        }
        if (Input.touchSupported) {
            if (Input.touchCount > 1) {
                return;
            }
        }
        else {
            if (eventData.button != PointerEventData.InputButton.Left) {
                return;
            }
        }


        var worldClickLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MapEditor.instance.selectedTool.EndToolDrag(worldClickLocation, null);
    }
}
