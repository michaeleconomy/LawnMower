﻿using System;
using UnityEngine;

public static class CameraController {
    private static float z = -10;
    private static float maxZoom = 20f;
    private static float minZoom = 1f;

    public static void CenterOn(GameObject gameObject) {
        MoveTo(gameObject.transform.position);
    }

    public static void CenterOn(MonoBehaviour monoBehaviour) {
        CenterOn(monoBehaviour.gameObject);
    }

    public static void Center() {
        Camera.main.transform.localPosition = new Vector3(0, 0, z);
    }

    public static void MoveTo(Vector2 position) {
        Camera.main.transform.position = new Vector3(position.x, position.y, z);
    }

    public static void FocusOn(MonoBehaviour monoBehaviour) {
        Camera.main.transform.SetParent(monoBehaviour.transform);
        Center();
    }

    public static void Detach() {
        Camera.main.transform.SetParent(null);
    }



    public static void ZoomRelative(float amount) {
        var newSize = Camera.main.orthographicSize * amount;
        newSize = Mathf.Min(newSize, maxZoom);
        newSize = Mathf.Max(newSize, minZoom);
        Camera.main.orthographicSize = newSize;
    }


    public static void Zoom(float newSize) {
        newSize = Mathf.Min(newSize, maxZoom);
        newSize = Mathf.Max(newSize, minZoom);
        Camera.main.orthographicSize = newSize;
    }

    public static void EditAngle(MonoBehaviour item) {
        var itemPos = item.transform.position;

        MoveTo(new Vector2(itemPos.x, itemPos.y - 0.95f));
        Zoom(1.5f);
    }
}
