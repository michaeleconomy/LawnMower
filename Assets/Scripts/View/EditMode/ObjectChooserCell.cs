﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class ObjectChooserCell : MonoBehaviour, IPointerClickHandler {
    public EditObjectInfo o;

    public void Initialize(EditObjectInfo newObject) {
        var image = GetComponent<Image>();

        o = newObject;
        if (o == null) {
            image.sprite = null;
            image.color = Color.clear;
            return;
        }
        image.color = Color.white;

        if (o is PrefabInfo) {
            var prefabInfo = (PrefabInfo)o;
            var prefab = prefabInfo.managedPrefab;
            if (prefab == null) {
                Debug.LogWarning("prefab is null: " + prefabInfo.id + " " +
                                 prefabInfo.path);
                return;
            }
            if (prefab.transform.childCount == 0) {
                var spriteRenderer = prefab.GetComponent<SpriteRenderer>();
                if (spriteRenderer != null) {
                    image.sprite = spriteRenderer.sprite;
                }
                else {
                    Debug.LogError("don't know how to render " + prefab.name +
                                   ", it has no spriteRenderer");
                }
            }
            else {
                image.sprite = SpriteMunger.Munge(prefab);
            }
            return;
        }
        if (o is TileInfo) {
            var layeredTile = (TileInfo)o;
            image.sprite = layeredTile.PreviewSprite();
            return;
        }

        Debug.LogError("don't know how to render type: " + o.GetType());
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        if (o == null) {
            return; // nothing!
        }
        EditObjectPalette.instance.SelectObject(o);
        MapEditor.instance.SelectBrush();
    }

    public static ObjectChooserCell Create(EditObjectInfo o, Transform parent) {
        var newCell = new GameObject();
        newCell.transform.SetParent(parent, false);
        var image = newCell.AddComponent<Image>();
        image.preserveAspect = true;
        var objectChooserCell = newCell.AddComponent<ObjectChooserCell>();
        objectChooserCell.Initialize(o);
        return objectChooserCell;
    }
}
