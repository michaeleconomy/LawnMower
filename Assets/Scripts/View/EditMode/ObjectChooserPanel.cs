﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ObjectChooserPanel : MonoBehaviour, IInitializedMenu {

    public GameObject objectGrid;
    public GameObject upButton;
    public Text pathText;
    public Transform filterButtons;
    public Button filterButtonPrefab;
    public string path;
    public string basePath;

    public void Initialize() {
        path = "";
        basePath = path;
        LoadForPath();
    }

    public void Filter(string _path) {
        path = path + _path + "/";
        LoadForPath();
    }

    private static Regex lastDirectory = new Regex(@"[\s\w]+/$");

    public void Up() {
        if (path == basePath) {
            throw new Exception("can't go up from basePath!");
        }
        path = lastDirectory.Replace(path, "");
        LoadForPath();
    }

    private void LoadForPath() {
        pathText.text = path;
        upButton.SetActive(path != basePath);

        filterButtons.DeleteChildren();
        objectGrid.transform.DeleteChildren();
        var pathInfo = EditObjectManager.instance.GetAllForPath(path);
        var sortedInfos = pathInfo.infos.OrderBy((i) => {
            var index = EditObjectPalette.instance.recentObjects.IndexOf(i);
            return index == -1 ? 99999 : index;
        });
        foreach (var o in sortedInfos) {
            ObjectChooserCell.Create(o, objectGrid.transform);
        }
        foreach (var subDirectory in pathInfo.subDirectories) {
            var button = Instantiate(filterButtonPrefab, filterButtons, false);
            button.GetComponentInChildren<Text>().text = subDirectory;
            button.onClick.AddListener(delegate { Filter(subDirectory); });
        }
    }
}
