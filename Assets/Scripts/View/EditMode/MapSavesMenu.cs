﻿using System;
using System.Linq;
using UnityEngine;

public class MapSavesMenu : MonoBehaviour, IInitializedMenu {
    public GameObject savesList;
    public GameObject saveListItemPrefab;

    public void Initialize() {
        savesList.transform.DeleteChildren();
        foreach (var mapSave in MapSaveManager.instance.mapSaves) {
            var listItem =
                Instantiate(saveListItemPrefab, savesList.transform, false);
            listItem.GetComponent<MapSaveListItem>().Initialize(mapSave);
        }
    }
}
