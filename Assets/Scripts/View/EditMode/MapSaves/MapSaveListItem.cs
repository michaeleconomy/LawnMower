﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MapSaveListItem : MonoBehaviour {
    public Text label;

    private S3Client.FileListItem file;

    public void Initialize(S3Client.FileListItem _file) {
        file = _file;
        label.text = "<b>" + file.fileName + "</b>\n" + file.lastModified;
    }

    public void Load() {
        UIDialog.Confirm("Are you sure you want to load (existing changes will be lost)?",
            () => {
            EditMenuController.instance.CloseAllMenus();
            MapSaveManager.instance.LoadFromS3(file.fileName);
        });
    }

    public void Delete() {
        UIDialog.Confirm("Are you sure you want to delete " + file.fileName + "?",
            () => {
            S3Client.instance.DeleteFile(file.fileName);
            Destroy(gameObject);
        }); 
    }
}
