﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AppearanceDropdown : MonoBehaviour {
    public Dropdown dropdown;
    public delegate void AppearanceDropdownChanged(int choice);
    public AppearanceDropdownChanged changeDelegate;
    public bool allowNone;

    private void Awake() {
        dropdown.onValueChanged.AddListener((val) => {
            if (allowNone) {
                val--;
            }
            changeDelegate(val);
        });
    }

    public void Initialize(bool _allowNone, int selected, List<string> choices) {
        if (choices.Count == 0) {
            gameObject.SetActive(false);
            return;
        }
        allowNone = _allowNone;
        gameObject.SetActive(true);
        var options = new List<string>();
        if (allowNone) {
            options.Add("none");
        }
        options.AddRange(choices);
        dropdown.ClearOptions();
        dropdown.AddOptions(options);
        var value = selected;
        if (allowNone) {
            value++;
        }
        dropdown.value = value;
    }

    public void Randomize() {
        if (!gameObject.activeSelf){
            return;
        }
        dropdown.value = Random.Range(0, dropdown.options.Count);
    }
}
