﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DetailsItemChooser : MonoBehaviour, IPointerClickHandler {
    public Item item;
    public bool added;

    public static DetailsItemChooser Create(Item item, bool added, Transform parent) {
        var gameObject = new GameObject();
        gameObject.transform.SetParent(parent, false);
        var image = gameObject.AddComponent<Image>();
        image.preserveAspect = true;
        image.sprite = item.GetComponent<SpriteRenderer>().sprite;
        var chooser = gameObject.AddComponent<DetailsItemChooser>();
        chooser.item = item;
        chooser.added = added;
        return chooser;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        var details = EditEntityDetails.instance;
        if (added) {
            details.RemoveItem(this);
            return;
        }
        details.AddItem(this);
    }
}
