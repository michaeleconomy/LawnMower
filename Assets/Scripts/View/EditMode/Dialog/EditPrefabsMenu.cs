﻿using System;
using UnityEngine;

public class EditPrefabsMenu : MonoBehaviour, IInitializedMenu {
    public Transform dialogsList;
    public PrefabManager prefabManager;
    public EditPrefabListItem listItemPrefab;

    public void Initialize() {
        dialogsList.DeleteChildren();
        var prefabs = World.instance.GetComponentsInChildren<ManagedPrefab>();

        foreach (var prefab in prefabs) {
            var listItem = Instantiate(listItemPrefab, dialogsList, false);
            var path = prefabManager.GetInfoById(prefab.prefabId).path;
            listItem.Initialize(prefab, path);
        }
    }
}
