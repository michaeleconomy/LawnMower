﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditMapsListItem : MonoBehaviour {
    public string mapName;
    public GameObject editButton;
    public GameObject resetButton;
    public Text mapNameText;

    public void Initialize(string _mapName) {
        mapName = _mapName;
        var isCurrent = mapName == EditMapManager.instance.currentMap;
        if (isCurrent) {
            mapNameText.text = ">>> " + mapName;
        }
        else {
            mapNameText.text = mapName;
        }
        editButton.SetActive(!isCurrent);
        resetButton.SetActive(isCurrent);
    }

    public void Edit() {
        EditMapManager.instance.SwitchToEdit(mapName);
        EditMenuController.instance.CloseAllMenus();
    }

    public void Reset() {
        UIDialog.Confirm("reset changes this map?", () => {
            EditMapManager.instance.ResetCurrentMap();
            EditMenuController.instance.CloseAllMenus();
        });
    }

    public void Delete() {
        UIDialog.Confirm("Are you sure you want to delete map: " + mapName, () => {
            var wasCurrent = mapName == EditMapManager.instance.currentMap;
            EditMapManager.instance.Delete(mapName);
            if (wasCurrent) {
                EditMenuController.instance.CloseAllMenus();
            }
            Destroy(gameObject);
        });
    }

    public void Rename() {
        UIDialog.PromptForString("Rename " + mapName + " to:", (newName) => {
            if (newName != null) {
                EditMapManager.instance.RenameMap(mapName, newName);
                Initialize(newName);
            }
        });
    }
}
