﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditTriggerListItem : MonoBehaviour {
    public Trigger trigger;
    public Text text;

    public void Initialize(Trigger trigger) {
        this.trigger = trigger;
        text.text = trigger.name;
    }

    public void Edit() {
        EditDialogPanel.instance.Edit(trigger.name, trigger.mapName);
    }

    public void Delete() {
        UIDialog.Confirm("Are you sure you want to delete " +
                         trigger.name + "?", () => {
            var menu = GetComponentInParent<EditTriggersMenu>();
            menu.triggerData.triggers.Remove(trigger);
            DialogData.instance.Remove(trigger.mapName, trigger.name);
            Destroy(gameObject);
        });
    }
}
