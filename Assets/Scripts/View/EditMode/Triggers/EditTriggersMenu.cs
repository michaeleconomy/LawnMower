﻿using System;
using UnityEngine;

public class EditTriggersMenu : MonoBehaviour, IInitializedMenu {
    public Transform triggerList;
    public TriggerData triggerData;
    public EditTriggerListItem listItemPrefab;
    private string map;

    public void Initialize() {
        map = EditMapManager.instance.currentMap;
        UpdateList();
    }

    public void ShowAnywhere() {
        map = TriggerData.anywhere;
        UpdateList();
    }

    public void Create() {
        UIDialog.PromptForString("Name for trigger?", (triggerName) => {
            if (string.IsNullOrEmpty(triggerName)) {
                return;
            }
            if (triggerData.triggers.Contains((t) => t.name == triggerName)) {
                UIDialog.Alert("Name is taken.");
                return;
            }
            if (!ManagedPrefab.ValidName(triggerName)) {
                UIDialog.Alert("Invalid name, must only contain word characters");
                return;
            }
            var trigger = new Trigger {
                mapName = map,
                name = triggerName
            };
            triggerData.triggers.Add(trigger);
            var listItem = Instantiate(listItemPrefab, triggerList, false);
            listItem.Initialize(trigger);
            EditDialogPanel.instance.Edit(trigger.name, trigger.mapName);
        });
    }

    public void UpdateList() {
        triggerList.DeleteChildren();
        foreach (var trigger in triggerData.GetForMap(map)) {
            var listItem = Instantiate(listItemPrefab, triggerList, false);
            listItem.Initialize(trigger);
        }
    }
}
