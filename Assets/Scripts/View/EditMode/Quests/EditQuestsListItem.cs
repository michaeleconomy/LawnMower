﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EditQuestsListItem : MonoBehaviour {
    public Text title;
    public Quest quest;

    public void EditQuest() {
        EditQuestsMenu.instance.EditQuest(quest);
    }

    public void Initialize(Quest _quest) {
        quest = _quest;
        title.text = quest.id + " - " + quest.title;
    }
}
