﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EditObjectsPanel : MonoBehaviour, IEditObjectPaletteDelegate {

    public Transform selectedContainer;
    public Transform recentContainer;
    public ObjectChooserPanel objectChooserPanel;

    private ObjectChooserCell selectedObjectCell;

    private void Start() {
        selectedObjectCell = ObjectChooserCell.Create(null, selectedContainer);

        EditObjectPalette.instance._delegate = this;
        ObjectsUpdated(EditObjectPalette.instance.selectedObject,
                       EditObjectPalette.instance.recentObjects);
    }

    public void ObjectsUpdated(EditObjectInfo selectedObject,
                               List<EditObjectInfo> recentObjects) {
        selectedObjectCell.Initialize(selectedObject);

        recentContainer.DeleteChildren();
        foreach (var o in recentObjects) {
            ObjectChooserCell.Create(o, recentContainer.transform);
        }

    }

    public void BrowseClick() {
        if (objectChooserPanel.gameObject.activeSelf) {
            objectChooserPanel.gameObject.SetActive(false);
            return;
        }
        objectChooserPanel.Initialize();
        objectChooserPanel.gameObject.SetActive(true);
    }
}
