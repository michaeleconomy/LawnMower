﻿using System;
using UnityEngine;

public class EditQuestsMenu : MonoBehaviour, IInitializedMenu {
    public static EditQuestsMenu instance;
    public GameObject questsList;
    public EditQuestsListItem questListItemPrefab;

    public EditQuestPanel editQuestPanel;

    private void Awake() {
        instance = this;
    }

    public void AddQuest(Quest q) {
        var listItem = Instantiate(questListItemPrefab, questsList.transform, false);
        listItem.Initialize(q);
    }

    public void EditQuest(Quest q) {
        editQuestPanel.Display(q);
    }

    public void NewQuest() {
        editQuestPanel.DisplayNew();
    }

    public void Initialize() {
        questsList.transform.DeleteChildren();
        foreach (var quest in QuestManager.instance.questData.quests) {
            AddQuest(quest);
        }
    }
}
