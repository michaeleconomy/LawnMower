﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditQuestPanel : MonoBehaviour {
    public bool isNew;
    public Quest quest;

    public Text title;
    public Text doneButtonText;

    public InputField idField;
    public InputField titleField;
    public InputField descriptionField;
    public Transform objectivesList;
    public EditQuestObjectiveListItem objectivePrefab;
    public InputField xpField;


    public void DisplayNew() {
        quest = null;
        isNew = true;
        idField.text = "";
        idField.interactable = true;
        titleField.text = "";
        descriptionField.text = "";
        xpField.text = "";
        doneButtonText.text = "Create";
        title.text = "New Quest";
        objectivesList.DeleteChildren();
        AddObjective();
        gameObject.SetActive(true);
    }

    public EditQuestObjectiveListItem AddObjective() {
        return Instantiate(objectivePrefab, objectivesList, false);
    }

    public void AddObjectiveUI() {
        AddObjective();
    }

    public void Display(Quest q) {
        isNew = false;
        quest = q;
        idField.text = q.id;
        idField.interactable = false;
        titleField.text = q.title;
        descriptionField.text = q.description;
        objectivesList.DeleteChildren();
        foreach (var objective in quest.objectives) {
            AddObjective().Initialize(objective);
        }
        xpField.text = q.xp.ToString();
        doneButtonText.text = "Done";
        title.text = "Edit Quest";
        gameObject.SetActive(true);
    }


    public void Done() {
        if (isNew) {
            quest = new Quest {
                id = idField.text
            };
            if (quest.id == "") {
                FloatyMessage.Display("id required");
                return;
            }
            if (QuestManager.instance.questsById.ContainsKey(quest.id)) {
                FloatyMessage.Display("id already in use: " + quest.id);
                return;
            }
        }
        if (titleField.text == "") {
            FloatyMessage.Display("title required");
            return;
        }
        quest.title = titleField.text;

        if (descriptionField.text == "") {
            FloatyMessage.Display("description required");
            return;
        }
        quest.description = descriptionField.text;

        if (!int.TryParse(xpField.text, out quest.xp)) {
            FloatyMessage.Display("couldn't parse xp amount: " + xpField.text);
            return;
        }
        var objectiveUIs =
            objectivesList.GetComponentsInChildren<EditQuestObjectiveListItem>();
        if (objectiveUIs.Length == 0) {
            FloatyMessage.Display("objectives are required");
            return;
        }
        var objectives = new List<Quest.Objective>();
        foreach (var objectiveUI in objectiveUIs) {
            var validatedObjective = objectiveUI.GetObjective();
            if (validatedObjective.error != null) {
                FloatyMessage.Display(validatedObjective.error);
                return;
            }
            objectives.Add(validatedObjective.objective);
        }
        quest.objectives = objectives;
        if (isNew) {
            QuestManager.instance.Add(quest);
            EditQuestsMenu.instance.AddQuest(quest);
        }
        gameObject.SetActive(false);
    }

    public void Cancel() {
        gameObject.SetActive(false);
    }

    public void Delete() {
        if (isNew) {
            gameObject.SetActive(false);
            return;
        }

        UIDialog.Confirm("Are you sure you want to delete this quest?", () => {
            QuestManager.instance.Remove(quest.id);
            EditQuestsMenu.instance.Initialize();
            gameObject.SetActive(false);
        });

    }

}
