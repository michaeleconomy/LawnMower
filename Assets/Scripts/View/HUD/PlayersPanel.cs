﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersPanel : MonoBehaviour, IPartyMemberDelegate {
    public static PlayersPanel instance;

    private void Awake() {
        instance = this;
        transform.DeleteChildren();
    }

    private void Start() {
        foreach (var pc in TurnManager.instance.party) {
            PlayerAdded(pc);
        }
        TurnManager.instance.AddPartyMemberDelegate(this);
    }

    public PlayerPanel playerPanelPrefab;

    public void PlayerAdded(PlayerCharacter player) {
        var playerPanel = Instantiate(playerPanelPrefab, transform, false);
        playerPanel.SetPlayer(player);

        SetSize();
    }

    private void SetSize() {
        var rectTransform = (RectTransform)transform;
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, TurnManager.instance.party.Count * 110);
    }

    public void PlayerRemoved(PlayerCharacter player) {
        for (var i = 0; i < transform.childCount; i++) {
            var childTransform = transform.GetChild(i);
            var playerPanel = childTransform.GetComponent<PlayerPanel>();
            if (player == playerPanel.player) {
                Destroy(playerPanel.gameObject);
                break;
            }
        }
        SetSize();
    }
}
