﻿using System;
using UnityEngine;

public class TurnIndicator : MonoBehaviour {
    public static TurnIndicator instance;
    public PlayersPanel playersPanel;
    private Character currentActiveCharacter;
    private Color selectedOutlineColor = new Color(1f, 1f, 1f, 0.5f);

    private void Awake() {
        instance = this;
    }

    public void TurnUpdated(Character activeCharacter) {
        Debug.Log("it is now " + activeCharacter.name + "'s turn, taking turn");
        if (currentActiveCharacter != null) {
            currentActiveCharacter.HideOutline();
        }

        var playerPanels = playersPanel.GetComponentsInChildren<PlayerPanel>();
        foreach (var playerPanel in playerPanels) {
            playerPanel.Selected(playerPanel.player == activeCharacter);
        }

        activeCharacter.EnableOutline(selectedOutlineColor);
        currentActiveCharacter = activeCharacter;
    }
}
