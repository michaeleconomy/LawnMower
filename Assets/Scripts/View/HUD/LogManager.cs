﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LogManager : MonoBehaviour, ICombatStatusDelegate {

    private static LogManager instance;

    public Text log;
    public ScrollRect logScroller;
    public Text miniLog;
    public ScrollRect miniLogScroller;


    private void Awake() {
        instance = this;
    }

    private void Start() {
        TurnManager.instance.AddCombatStatusDelegate(this);
    }

    public static void ScrollJournal() {
        instance.StartCoroutine(instance.ScrollLogsLater());
    }

    private IEnumerator ScrollLogsLater() {
        //Do this after one frame so the text has time to appear first
        yield return null;
        logScroller.verticalNormalizedPosition = 0f;
        miniLogScroller.verticalNormalizedPosition = 0f;
    }

    public static void Log(string s) {
        instance.AppendLog(s);
    }

    public static void Clear() {
        instance.log.text = "\n\n\n\n\n\n";
    }

    private void AppendLog(string s) {
        log.text = log.text + "\n" + s;
        miniLog.text = log.text;
        ScrollJournal();
    }

    void ICombatStatusDelegate.CombatStarted() {
        AppendLog("Entering Combat.");
    }

    void ICombatStatusDelegate.CombatEnded() {
        AppendLog("Combat has ended.");
    }
}
