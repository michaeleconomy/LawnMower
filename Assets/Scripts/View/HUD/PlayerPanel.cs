﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerPanel : MonoBehaviour, IPlayerHealthDelegate {
    public PlayerCharacter player;
    public GameObject selectedBox;
    public RectTransform healthBar;
    private Color selectedColor = Color.white;

    private Color unselectedColor = new Color(1f, 1f, 1f, 0.4f);


    private void OnDestroy() {
        if (player != null) {
            player.healthDelegate = null;
        }
    }

    public void PlayerHealthChanged(PlayerCharacter character) {
        if (!character.dead) {
            healthBar.gameObject.SetActive(true);
            var healthPercent = ((float)character.hp) / character.hpMax;
            healthBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                                                100 * healthPercent);
            var healthBarImage = healthBar.GetComponent<Image>();
            if (healthPercent > 0.4f) {
                healthBarImage.color = Color.green;
            }
            else if (healthPercent > 0.2f) {
                healthBarImage.color = Color.yellow;
            }
            else {
                healthBarImage.color = Color.red;
            }
            return;
        }

        healthBar.gameObject.SetActive(false);
        //TODO show a ghost photo
    }

    public void SetPlayer(PlayerCharacter _player) {
        player = _player;

        player.healthDelegate = this;
        var image = GetComponent<Image>();
        image.sprite = SpriteMunger.MungeHead(player);

        PlayerHealthChanged(player);
        Selected(TurnManager.instance.activeCharacter == player);
    }

    public void Selected(bool selected) {
        var image = GetComponent<Image>();
        if (selected) {
            image.color = selectedColor;
            selectedBox.SetActive(true);
        }
        else {
            image.color = unselectedColor;
            selectedBox.SetActive(false);
        }
    }
}
