﻿using System;
using System.Collections;
using UnityEngine;

public class ActionController : MonoBehaviour, IPlayerActionDelegate {
    public static ActionController instance;
    private bool takingActions;

    private PlayerCharacter activePlayerCharacter;

    private void Awake() {
        instance = this;
        PlayerCharacter.playerActionDelegate = this;
    }

    public void GetPlayerActions(PlayerCharacter player) {
        activePlayerCharacter = player;
        CameraController.FocusOn(activePlayerCharacter);
        //TODO show their attacks
        //TODO show spells
        //TODO show specials
        StartTakingActions();
    }

    public void StartTakingActions() {
        MenuController.instance.TakingActions(true);
        ActionOverlay.instance.Show(activePlayerCharacter);
        takingActions = true;
    }

    public IEnumerator DoAction(IEnumerator action) {
        if (!takingActions) {
            yield break;
        }
        StopTakingActions();
        yield return StartCoroutine(action);
        StartTakingActions();
    }

    public void StopTakingActions() {
        takingActions = false;
        ActionOverlay.instance.Clear();
        MenuController.instance.TakingActions(false);
    }

    void ICharacterActionDelegate.TurnEnd(Character character) {
        StopTakingActions();
    }

    void ICharacterActionDelegate.AvailibleActionsChanged(Character character) {
        //TODO  grey out the actions that are no longer availible
    }

    public void EndTurn() {
        activePlayerCharacter.actions.EndTurn();
    }

    //TODO - lock the UI while the character is doing something
}
