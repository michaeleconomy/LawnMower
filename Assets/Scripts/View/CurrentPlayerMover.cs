﻿using System;
using System.Collections;
using UnityEngine;

public static class CurrentPlayerMover {

    public static IEnumerator Move(GameObject target, float range, bool allowRun = true) {
        var pointer = World.instance.movePointer;
        pointer.transform.position = target.transform.position.RoundPosition();

        var player = TurnManager.instance.CurrentPlayer();
        if (player == null) {
            Debug.LogWarning("CurrentPlayerMover - Current player is null....");
            yield break;
        }

        if (player.movement.IsInRange(target, range)) {
            //Debug.Log("CurrentPlayerMover - already there");
            yield break;
        }

        //if we can move there
        if (CanGetThere(player, target, range, allowRun)) {
            //Debug.Log("CurrentPlayerMover - can get there, moving");
            pointer.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 0.5f);
            pointer.SetActive(true);
            if (TurnManager.inCombat) {
                yield return player.StartCoroutine(player.movement.MoveInRange(target, range));
            }
            else {
                yield return player.StartCoroutine(player.movement.MultiTurnMoveInRange(target, range));
            }
            pointer.SetActive(false);
            yield break;
        }

        //if we can't move show a red pointer
        LogManager.Log("Can not move there.");
        pointer.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 0.5f);
        pointer.SetActive(true);
        yield return new WaitForSeconds(1f);

        //TODO bug where clicknig before this happens causes pointer to disappear
        pointer.SetActive(false); 
    }


    public static IEnumerator MoveAndOpenInventory(GameObject gameObject) {
        var player = TurnManager.instance.CurrentPlayer();
        if (player == null) {
            Debug.LogWarning("CurrentPlayerMover - Current player is null....");
            yield break;
        }

        yield return player.StartCoroutine(Move(gameObject, 0f));
        if (player.movement.IsInRange(gameObject, 0f)) {
            MenuController.instance.ShowInventory();
        }
    }

    private static bool CanGetThere(PlayerCharacter player, GameObject target, float range, bool allowRun) {
        if(TurnManager.inCombat) {
            if (player.movement.CanMoveInRange(target, range)) {
                return true;
            }
            if (allowRun &&
                    player.actions.HasStandardAction() &&
                    player.movement.CanRunInRange(target, range)) {
                player.actions.TakeRunAction();
                return true;
            }
            return false;
        }

        //not in combat
        return player.movement.CanMoveInRange(target, range, 300);
    }
}
