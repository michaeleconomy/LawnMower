﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
    public static MenuController instance;

    public GameObject menu;
    public GameObject menuButton;
    public GameObject journalMenu;
    public GameObject questsMenu;
    public GameObject helpMenu;
    public GameObject inventoryMenu;
    public GameObject characterMenu;
    public GameObject spellMenu;

    //hud
    public GameObject miniLogContainer;
    public GameObject actionPanel;
    public GameObject playerPanel;

    public PlayerCharacter menuCharacter;

    private bool takingActions = false;

    public void TakingActions(bool takingActions) {
        this.takingActions = takingActions;
        ToggleActionPanel();
    }

    private void ToggleActionPanel() {
        actionPanel.SetActive(takingActions && !MenusOpen());
    }

    private void Awake() {
        instance = this;
    }

    public void ToggleMenu() {
        if (MenusOpen()) {
            CloseAllMenus();
            return;
        }
        menu.SetActive(true);
        HideHud();
    }

    public void CloseAllMenus() {
        menuCharacter = null;
        HideOtherMenus();
        ShowHud();
        menuButton.SetActive(true);
    }

    private void ShowHud() {
        miniLogContainer.SetActive(true);
        LogManager.ScrollJournal();
        ToggleActionPanel();
        playerPanel.SetActive(true);
    }

    private void HideHud(bool includePlayerPanel = false) {
        miniLogContainer.SetActive(false);
        ToggleActionPanel();
        playerPanel.SetActive(!includePlayerPanel);
    }

    public void ShowJournal() {
        ShowMenu(journalMenu);
    }

    public void ShowQuests() {
        ShowMenu(questsMenu);
    }

    public void ShowCharacter() {
        ShowMenu(characterMenu);
    }

    public void ShowInventory() {
        ShowMenu(inventoryMenu);
    }

    private void ShowMenu(GameObject m) {
        HideOtherMenus();
        HideHud();
        m.SetActive(true);
        var initializedMenu = m.GetComponent<IInitializedMenu>();
        if (initializedMenu != null) {
            initializedMenu.Initialize();
        }

        var playerInitializedMenu = m.GetComponent<IPlayerInitializedMenu>();
        if (playerInitializedMenu != null) {
            if (menuCharacter == null) {
                menuCharacter = TurnManager.instance.CurrentPlayer();
            }
            if (menuCharacter == null) {
                menuCharacter = TurnManager.instance.Players()[0];
            }
            playerInitializedMenu.Initialize(menuCharacter);
        }
    }

    public void ShowSpells() {
        ShowMenu(spellMenu);
    }

    public void ShowHelp() {
        ShowMenu(helpMenu);
    }

    public void MainMenu() {
        UIDialog.Confirm("Go to main menu? all unsaved progress will be lost.",
                         () => {
            SceneManager.LoadScene("Scenes/MainMenu");
        });
    }

    private bool MenusOpen() {
        return menu.activeSelf ||
                   journalMenu.activeSelf ||
                   questsMenu.activeSelf ||
                   inventoryMenu.gameObject.activeSelf ||
                   characterMenu.gameObject.activeSelf ||
                   spellMenu.gameObject.activeSelf ||
                   helpMenu.activeSelf;
    }

    private void HideOtherMenus() {
        menu.SetActive(false);
        journalMenu.SetActive(false);
        questsMenu.SetActive(false);
        inventoryMenu.gameObject.SetActive(false);
        characterMenu.gameObject.SetActive(false);
        spellMenu.gameObject.SetActive(false);
        helpMenu.SetActive(false);
    }

    public void ShowEverything() {
        ShowHud();
        menuButton.SetActive(true);
    }

    public void HideEverything() {
        CloseAllMenus();
        HideHud(true);
        menuButton.SetActive(false);
    }
}