﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIInventoryItem : UIInventoryGameEntity, 
                                IBeginDragHandler, IDragHandler, IEndDragHandler {

    private UIInventorySlot oldSlot;


    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        UIInventoryItemDetails.instance.Hide();
        oldSlot = transform.parent.GetComponent<UIInventorySlot>();
        transform.SetParent(UIInventoryMenu.instance.temporaryDragSpot);
        transform.position = eventData.position;

        //TODO highlight the spots it can go in
        //if (gameEntity is Equipment) {
        //    var equipment = (Equipment)gameEntity;
        //}
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        transform.position = eventData.position;
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        //TODO remove any highlights from above
        var newSlot = GetSlot(eventData);

        //TODO if you drop on the character photo - try and equip it

        if (newSlot == null) {
            transform.SetParent(oldSlot.transform);
            oldSlot = null;
        }
        else {
            var reason = ReasonCantPutInSlot(newSlot);
            if (reason != null) {
                FloatyMessage.Display(reason);
                transform.SetParent(oldSlot.transform);
                oldSlot = null;
            }
            else {
                MoveItemToSlot(newSlot);
            }
        }
        transform.localPosition = Vector2.zero;
        UIInventoryItemDetails.instance.Show(this, true);
    }

    public void Drop() {
        UIInventoryMenu.instance.ShowGround();
        var newSlot = UIInventoryMenu.instance.FirstFreeGroundSlot();
        PlaceItemInEmptySlot(newSlot.GetComponent<UIInventorySlot>());
    }

    private void MoveItemToSlot(UIInventorySlot newSlot) {
        if (newSlot.transform.childCount > 0) {
            RemoveItemInSlot(newSlot);
        }
        PlaceItemInEmptySlot(newSlot);
    }

    private void RemoveItemInSlot(UIInventorySlot newSlot) {
        var oldUIItem = newSlot.GetComponentInChildren<UIInventoryItem>();


        UIInventorySlot freeSlot;
        if (oldSlot.ground) {
            freeSlot = UIInventoryMenu.instance.FirstFreeGroundSlot();
        }
        else if(oldSlot.inventory) {
            freeSlot = UIInventoryMenu.instance.FirstFreeInventorySlot();
        }
        else if (oldSlot.container) {
            freeSlot = UIInventoryMenu.instance.FirstFreeContainerSlot();
        }
        else {
            throw new Exception("unhandled slot type!");
        }
        oldUIItem.PlaceItemInEmptySlot(freeSlot);
    }

    public void PlaceItemInEmptySlot(UIInventorySlot newSlot) {
        var item = (Item) gameEntity;
        if (oldSlot == null) {
            oldSlot = GetComponentInParent<UIInventorySlot>();
        }
        transform.SetParent(newSlot.transform);
        if (oldSlot.isEquipmentSlot && !newSlot.isEquipmentSlot) {
            UIInventoryMenu.instance.character.Unequip((Equipment)gameEntity);
            UIInventoryMenu.instance.UpdatePreview();
        }

        //TODO - reorder inventory and equipment slots
        //TODO - do i want to reorder ground slots?   probably not

        if (oldSlot.inventory && !newSlot.inventory) {
            UIInventoryMenu.instance.character.Remove(item);
        }
        else if (oldSlot.container && !newSlot.container) {
            UIInventoryMenu.instance.container.Remove(item);
        }
        else if (oldSlot.ground && !newSlot.ground) {
            UIInventoryMenu.instance.TogglePickUpAllButton();
        }

        if (!oldSlot.ground && newSlot.ground) {
            UIInventoryMenu.instance.pickUpAllButton.SetActive(true);
        }
        else if (!oldSlot.inventory && newSlot.inventory) {
            UIInventoryMenu.instance.character.Add(item);
        }
        else if (!oldSlot.container && newSlot.container) {
            UIInventoryMenu.instance.container.Add(item);
        }

        if (!oldSlot.isEquipmentSlot && newSlot.isEquipmentSlot) {
            UIInventoryMenu.instance.character.Equip((Equipment)item);
            UIInventoryMenu.instance.UpdatePreview();
        }
        UIInventoryMenu.instance.UpdateWeightText();
        oldSlot = null;
    }

    private string ReasonCantPutInSlot(UIInventorySlot newSlot) {
        if (newSlot.isEquipmentSlot) {
            if (!(gameEntity is Equipment)) {
                return "Item is not equipable";
            }
            var equipment = (Equipment)gameEntity;
            if (equipment.slot != newSlot.equipmentSlot) {
                return equipment.DisplayName() + " goes in " + equipment.slot;
            }
        }
        if (newSlot.container) {
            var container = UIInventoryMenu.instance.container;
            if (gameEntity.isContainer) {
                if (container == gameEntity) {
                    return "Can't put a container inside of itself.";
                }
                var parentContainers =
                    container.GetComponentsInParent<GameEntity>();
                foreach (var parentContainer in parentContainers) {
                    if (parentContainer == gameEntity) {
                        return "Can't put a container inside of itself.";
                    }
                }
            }

            if (gameEntity is Item) {
                var item = (Item)gameEntity;
                if (item.questItem) {
                    return "The " + item.DisplayName() + " doesn't fit in the " +
                                        container.DisplayName();
                }
            }
        }
        if (newSlot.ground) {
            if (gameEntity is Item) {
                var item = (Item)gameEntity;
                if (item.questItem) {
                    return "You don't want to put down the " +
                        item.DisplayName();
                }
            }

        }
        //TODO check on weight?
        return null;
    }

    private UIInventorySlot GetSlot(PointerEventData eventData) {
        var hitObjects = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, hitObjects);

        foreach (var hitObject in hitObjects){
            var slot = hitObject.gameObject.GetComponent<UIInventorySlot>();
            if (slot != null) {
                return slot;
            }
        }
        return null;
    }
}
