﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryMenu : MonoBehaviour, IPlayerInitializedMenu {
    public static UIInventoryMenu instance;

    public PlayerCharacter character;

    public Transform temporaryDragSpot;
    public GameObject uiInventorySlotPrefab;
    public GameObject uiInventoryItemPrefab;
    public GameObject uiInventoryGameEntityPrefab;

    public Transform inventoryGrid;
    public Image characterImage;
    public Transform armorSlot;
    public Transform helmSlot;
    public Transform glovesSlot;
    public Transform shoesSlot;
    public Transform beltSlot;
    public Transform heldSlots;
    public Transform ringSlots;
    public Transform necklaceSlots;
    public Transform braceletSlots;

    public GameObject groundContainer;
    public Transform groundSlots;
    public GameObject pickUpAllButton;

    public GameEntity container;
    public Text containerNameText;
    public GameObject containerContainer;
    public Transform containerSlots;

    public Text nameText;
    public Text weightText;
    public Text acText;


    private void Awake() {
        instance = this;
    }

    public void Initialize(PlayerCharacter c) {
        if (UIInventoryItemDetails.instance != null){
            UIInventoryItemDetails.instance.Hide();
        }
        character = c;
        UpdatePreview();
        acText.text = character.ArmorClass().ToString();
        UpdateWeightText();
        nameText.text = character.name;

        var uiGameEntities = transform.GetComponentsInChildren<UIInventoryGameEntity>(true);
        foreach (var uiGameEntity in uiGameEntities) {
            Destroy(uiGameEntity.gameObject);
        }

        var equipmentToExclude = new HashSet<Item>();

        var equipment = character.equipment;

        EquipmentToSlot(equipment.armor, armorSlot, equipmentToExclude);
        EquipmentToSlot(equipment.belt, beltSlot, equipmentToExclude);
        EquipmentToSlot(equipment.gloves, glovesSlot, equipmentToExclude);
        EquipmentToSlot(equipment.helm, helmSlot, equipmentToExclude);
        EquipmentToSlot(equipment.shoes, shoesSlot, equipmentToExclude);
        EquipmentToSlot(equipment.bracelets, braceletSlots, equipmentToExclude);
        EquipmentToSlot(equipment.hands, heldSlots, equipmentToExclude);
        EquipmentToSlot(equipment.necklaces, necklaceSlots, equipmentToExclude);
        EquipmentToSlot(equipment.rings, ringSlots, equipmentToExclude);

        //TODO - can get fancy here AdjustEmptySlots();
        var inventoryItems =
            character.contents.Where(a => !equipmentToExclude.Contains(a)).ToList();

        FillSlots(inventoryGrid, inventoryItems);

        InitializeGround();

        CloseContainer();
    }

    public void UpdatePreview() {
        characterImage.sprite = SpriteMunger.Munge(character);
    }

    public void UpdateWeightText() {
        weightText.text = character.ContentsWeight().ToString() + " ounces";
    }

    public void ShowGround() {
        groundContainer.SetActive(true);
        pickUpAllButton.SetActive(true);
    }

    public void PickUpAll() {
        var groundUIItems = groundSlots.GetComponentsInChildren<UIInventoryItem>(true);
        foreach (var groundUIItem in groundUIItems) {
            var newSlot = FirstFreeInventorySlot();
            groundUIItem.PlaceItemInEmptySlot(newSlot);
        }
        pickUpAllButton.SetActive(false);
    }

    private void InitializeGround() {
        var groundEntities = GroundEntities();
        if (groundEntities.Count == 0) {
            groundContainer.SetActive(false);
            return;
        }
        pickUpAllButton.SetActive(true);
        groundContainer.SetActive(true);
        FillSlots(groundSlots, groundEntities);
    }
    
    private void FillSlots(Transform slots, List<Item> items) {
        FillSlots(slots, items.Cast<GameEntity>().ToList());
    }

    private void FillSlots(Transform slots, List<GameEntity> gameEntities) {
        var i = 0;
        foreach (var entity in gameEntities) {
            if (i >= slots.childCount) {
                throw new Exception("not enough ground slots!");
            }
            var slot = slots.GetChild(i);
            i++;
            InitializeGameEntityToSlot(entity, slot);
        }
    }

    public void TogglePickUpAllButton() {
        pickUpAllButton.SetActive(StillItemsOnGround());
    }

    public void CloseContainer() {
        containerContainer.SetActive(false);
        container = null;
    }

    public void OpenContainer(GameEntity c) {
        container = c;
        var uiItems = containerContainer.GetComponentsInChildren<UIInventoryItem>(true);
        foreach (var uiItem in uiItems) {
            Destroy(uiItem.gameObject);
        }
        FillSlots(containerSlots, c.contents);
        containerNameText.text = c.name;
        containerContainer.SetActive(true);
    }

    private bool StillItemsOnGround() {
        return groundSlots.GetComponentInChildren<UIInventoryItem>() != null;
    }

    private List<GameEntity> GroundEntities() {
        var layerMask = LayerMask.GetMask("items");
        var contacts =
            Physics2D.OverlapPointAll(character.transform.position, layerMask);
        var groundEntities = new List<GameEntity>();
        foreach (var contact in contacts) {
            var gameEntity = contact.GetComponent<GameEntity>();
            if (gameEntity != null && gameEntity.isContainer || gameEntity is Item) {
                groundEntities.Add(gameEntity);
            }
        }
        return groundEntities;
    }

    private void EquipmentToSlot(Equipment equipment, Transform slot,
                                 HashSet<Item> equipmentToExclude) {
        if (equipment != null) {
            equipmentToExclude.Add(equipment);
            InitializeGameEntityToSlot(equipment, slot);
        }
    }

    private void EquipmentToSlot(List<Equipment> equipment, Transform slots,
                                 HashSet<Item> equipmentToExclude) {
        var i = 0;
        foreach (var e in equipment) {
            if (i >= slots.childCount) {
                throw new Exception("Not enough slots of type: " + e.slot);
            }
            EquipmentToSlot(e, slots.GetChild(i), equipmentToExclude);
            i++;
        }
    }

    private void InitializeGameEntityToSlot(GameEntity gameEntity, Transform slot) {
        GameObject uiItem;
        if (gameEntity is Item) {
            uiItem = Instantiate(uiInventoryItemPrefab);
        }
        else {
            uiItem = Instantiate(uiInventoryGameEntityPrefab);
        }
        uiItem.GetComponent<UIInventoryGameEntity>().InitForItem(gameEntity);
        var uiItemTransform = (RectTransform)uiItem.transform;
        uiItemTransform.SetParent(slot, false);

        //TODO is this fixed?
        //I don't know why this is getting fucked up
        //uiItemTransform.localScale = Vector3.one;
    }

    public UIInventorySlot FirstFreeInventorySlot() {
        return FirstFreeSlot(inventoryGrid);
    }

    public UIInventorySlot FirstFreeGroundSlot() {
        return FirstFreeSlot(groundSlots);
    }

    public UIInventorySlot FirstFreeContainerSlot() {
        return FirstFreeSlot(containerSlots);
    }

    private UIInventorySlot FirstFreeSlot(Transform slotContainer) {
        for (var i = 0; i < slotContainer.childCount; i++) {
            var slot = slotContainer.GetChild(i);
            if (slot.childCount == 0) {
                return slot.GetComponent<UIInventorySlot>();
            }
        }
        throw new Exception("out of slots!");
    }

    //private void AdjustEmptySlots() {
    //    var idealSlots = character.inventory.Count + 1;
    //    //TODO - one free row, and a minimum.

    //    while (inventoryGrid.childCount < idealSlots) {
    //        AddSlot();
    //    }
    //    while (inventoryGrid.childCount > idealSlots) {
    //        var lastSlot = inventoryGrid.GetChild(inventoryGrid.childCount - 1);
    //        Destroy(lastSlot);
    //    }

    //}


    //private Transform AddSlot() {
    //    var newSlot = Instantiate<GameObject>(uiInventorySlotPrefab).transform;
    //    newSlot.SetParent(inventoryGrid);
    //    return newSlot;
    //}
}
