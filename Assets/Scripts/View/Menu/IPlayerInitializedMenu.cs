﻿using System;
public interface IPlayerInitializedMenu {
    void Initialize(PlayerCharacter player);
}
