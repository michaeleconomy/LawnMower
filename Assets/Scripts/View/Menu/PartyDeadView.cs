﻿using System;
using System.Collections;
using UnityEngine;

public class PartyDeadView : MonoBehaviour, IPartyDeadDelegate {
    public GameObject view;
    public GameObject hud;
    public GameObject menuButton;

    private void Start() {
        TurnManager.instance.partyDeadDelegate = this;
    }

    void IPartyDeadDelegate.PartyDead() {
        hud.SetActive(false);
        menuButton.SetActive(false);
        MenuController.instance.CloseAllMenus();
        StartCoroutine(WaitAndShowDialog());

    }

    private IEnumerator WaitAndShowDialog() {
        yield return new WaitForSeconds(3);
        view.SetActive(true);
    }

    public void RespawnParty() {
        view.SetActive(false);
        Respawner.instance.RespawnParty();
        hud.SetActive(true);
        menuButton.SetActive(true);
    }
}
