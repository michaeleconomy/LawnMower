﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public abstract class TextMeshProLinkTouchHandler : MonoBehaviour, IPointerClickHandler {
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        var text = GetComponent<TextMeshProUGUI>();
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
        if (linkIndex != -1) { // was a link clicked?
            var linkInfo = text.textInfo.linkInfo[linkIndex];
            HandleTouch(linkInfo.GetLinkID());
        }
    }
    public abstract void HandleTouch(string linkId);
}
