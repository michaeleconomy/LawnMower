﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public SaveManager saveManager;
    public GameObject continueButton;
    public GameObject loadButton;

    private void Awake() {
        var hasSave = saveManager.HasSave();
        continueButton.SetActive(hasSave);
        loadButton.SetActive(hasSave);
    }

    public void Continue() {
        SaveManager.fileToLoad = saveManager.LastSaveFileName();
        SceneManager.LoadScene("Scenes/MainScene");
    }

    public void New() {
        SaveManager.fileToLoad = null;
        SceneManager.LoadScene("Scenes/MainScene");
    }

    public void Load() {
        //TODO
        throw new NotImplementedException();
    }

    public void Edit() {
        SceneManager.LoadScene("Scenes/EditScene");
    }
}
