﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class DialogPanel : MonoBehaviour {

    public Transform dialogList;
    public DialogRow dialogRowPrefab;
    public Transform choicesList;
    public Button choicePrefab;
    public ScrollRect scroller;
    private bool readyToProgress;
    private bool defaultButtonShown = false;
    private bool shouldAddWhitespace = false;


    Regex speakerRegex = new Regex(@"^(1|2|.*\:)");

    public void EndDialog() {
        gameObject.SetActive(false);
        MenuController.instance.ShowEverything();
    }

    public IEnumerator GetChoice(List<string> choices, DialogRunner.MakeChoice makeChoice) {
        choicesList.DeleteChildren();
        defaultButtonShown = false;
        for (var i = 0; i < choices.Count; i++) {
            var choice = choices[i];
            var button = Instantiate(choicePrefab, choicesList, false);
            button.transform.GetChild(0).GetComponent<Text>().text = choice;
            var choiceId = i;
            button.onClick.AddListener(() => makeChoice(choiceId));
        }
        yield return null;
        scroller.verticalNormalizedPosition = 0f;
    }

    public IEnumerator ShowDialog(string dialog) {
        var speakerMatch = speakerRegex.Match(dialog);
        ManagedPrefab speaker = null;
        string speakerName = null;
        if (shouldAddWhitespace) {
            shouldAddWhitespace = false;
            LogManager.Log("");
        }
        if (speakerMatch.Success) {
            speakerName = speakerMatch.Value;
            dialog = dialog.Substring(speakerName.Length);

            speakerName = speakerName.Replace(":", "");
            speaker = FindSpeaker(speakerName);

            if (speaker != null) {
                speakerName = speaker.DisplayName();
            }
            LogManager.Log(speakerName + ": " + dialog);
        }
        else {
            LogManager.Log(dialog);
        }

        var dialogRow = Instantiate(dialogRowPrefab, dialogList, false);
        dialogRow.Initialize(speaker, speakerName, dialog);
        readyToProgress = false;

        ShowDefaultButton();
        yield return null;
        scroller.verticalNormalizedPosition = 0f;

        while (!readyToProgress) {
            yield return null;
        }
    }

    private ManagedPrefab FindSpeaker(string speakerName) {
        switch (speakerName) {
            case "1":
                return PlayerCharacter.primaryPlayer;
            case "2":
                return DialogRunner.instance.speaker;
        }
        return World.instance.FindChildByName<ManagedPrefab>(speakerName);
    }

    private void ShowDefaultButton() {
        if (defaultButtonShown) {
            return;
        }
        defaultButtonShown = true;
        choicesList.DeleteChildren();
        var button = Instantiate(choicePrefab, choicesList, false);
        button.transform.GetChild(0).GetComponent<Text>().text = "Click to Continue";
        button.onClick.AddListener(() => readyToProgress = true);
    }

    public void StartDialog() {
        gameObject.SetActive(true);
        dialogList.DeleteChildren();
        defaultButtonShown = false;
        choicesList.DeleteChildren();
        MenuController.instance.HideEverything();
        shouldAddWhitespace = true;
    }
}
