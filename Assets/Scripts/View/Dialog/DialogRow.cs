﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class DialogRow : MonoBehaviour {
    public Image faceImage;
    public Transform faceSlot;
    public Text nameText;
    public Text dialogText;

    public void Initialize(ManagedPrefab speaker, string speakerName, string dialog) {
        if (speaker != null) {
            InitializeImage(speaker);
        }

        if (speakerName != null) {
            nameText.text = speakerName;
        }
        else {
            faceSlot.gameObject.SetActive(false);
        }
        dialogText.text = dialog;
    }

    private void InitializeImage(ManagedPrefab speaker) {
        Sprite sprite = null;
        if (speaker is Character) {
            var character = (Character)speaker;
            sprite = SpriteMunger.MungeHead(character);
        }
        if (sprite == null) {
            var spriteRenderer = speaker.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null) {
                sprite = spriteRenderer.sprite;
            }
        }
        if (sprite != null) {
            faceImage.sprite = sprite;
        }
        if (speaker == PlayerCharacter.primaryPlayer) {
            faceSlot.SetAsLastSibling();
            dialogText.alignment = TextAnchor.UpperRight;
        }
    }

}
