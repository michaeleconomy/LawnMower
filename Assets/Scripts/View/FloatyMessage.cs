﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FloatyMessage : MonoBehaviour {
    private static FloatyMessage instance;
    private static Coroutine hideCoroutine;
    private Text text;
    private RectTransform rectTransform;

    private void Awake() {
        instance = this;
        text = GetComponent<Text>();
        rectTransform = (RectTransform)transform;
    }

    public void DisplayMessage(string message) {
        Reposition();
        text.text = message;
        if (hideCoroutine != null) {
            StopCoroutine(hideCoroutine);
        }
        gameObject.SetActive(true);
        hideCoroutine = StartCoroutine(HideMessage());
    }

    private void Reposition() {
        var x = Input.mousePosition.x;
        var y = Input.mousePosition.y;
        var scaleFactor = GetComponentInParent<Canvas>().scaleFactor;

        var halfWidth = rectTransform.rect.xMax * scaleFactor;
        var halfHeight = rectTransform.rect.yMax * scaleFactor;

        //NOTE - this assumes the pivot point of the rect is in the center
        if (x - halfWidth < 0) {
            x = halfWidth;
        }
        else if (x + halfWidth >= Camera.main.pixelWidth) {
            x = Camera.main.pixelWidth - halfWidth;
        }

        if (y - halfHeight < 0) {
            y = halfHeight;
        }
        else if (y + halfHeight >= Camera.main.pixelHeight) {
            y = Camera.main.pixelWidth - halfHeight;
        }
        transform.position = new Vector3(x, y, transform.position.z);
    }

    public static void Display(string message) {
        instance.DisplayMessage(message);
    }

    public IEnumerator HideMessage(){
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
        hideCoroutine = null;
    }
}
