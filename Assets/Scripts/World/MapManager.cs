﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class MapManager : MonoBehaviour {

    public List<Tilemap> tilemaps;
    public World world;

    public TileManager tileManager;
    public PrefabManager prefabManager;
    public TileBase blockingTile;

    public MapData mapData;
    public string currentMap;
    const int maxLayersInPrefab = 50;
    public int prefabsLoaded = 0;

    public void LoadBaseMaps(List<MapSave> mapSaves, string startMap) {
        mapData.Reload(mapSaves, startMap);
        LoadBaseMap(startMap, false);
        CameraController.Center();
    }

    protected void LoadBaseMap(string mapName, bool onlyTiles) {
        Overlay.Show("Loading...");
        var mapSave = mapData.GetMap(mapName);
        ClearMap();
        currentMap = mapName;

        //tiles
        var bounds = mapSave.TileBounds();

        var mapSize = bounds.size.x * bounds.size.y;
        //var tiles = new TileBase[mapSize];
        for (var tilemapIndex = 0; tilemapIndex < tilemaps.Count; tilemapIndex++) {
            var tilemap = tilemaps[tilemapIndex];

            var tilemapOffset = tilemapIndex * mapSize;
            for (var tileIndex = 0; tileIndex < mapSize; tileIndex++) {
                var tileId = mapSave.tileLayout[tilemapOffset + tileIndex];
                var tile = tileManager.GetTileBaseById(tileId);
                //tiles[tileIndex] = tile;
                var position =
                    new Vector3Int(bounds.x + tileIndex % bounds.size.x,
                                   bounds.y + tileIndex / bounds.size.x, 0);
                tilemap.SetTile(position, tile);
            }

            //tilemap.SetTilesBlock(bounds, tiles);
        }

        FillInWalls(bounds, tilemaps[0], tilemaps[2]);

        if (!onlyTiles) {
            //prefabs
            LoadBasePrefabs(mapSave.prefabs);
        }

#if UNITY_EDITOR
        Undo.RecordObjects(tilemaps.ToArray(), "loaded map");
#endif
        Overlay.Hide();
    }

    private void FillInWalls(BoundsInt originalBounds, Tilemap ground, Tilemap walls) {
        if (World.inEditMode) {
            return;
        }
        var bounds = new BoundsInt(originalBounds.min - Vector3Int.one,
                                   originalBounds.size + (Vector3Int.one * 2));
        for (var x = bounds.xMin; x <= bounds.xMax; x++) {
            for (var y = bounds.yMin; y <= bounds.yMax; y++) {
                var v = new Vector3Int(x, y, 0);
                if (!ground.HasTile(v) && !walls.HasTile(v)) {
                    walls.SetTile(v, blockingTile);
                }
            }
        }
    }

    private void LoadBasePrefabs(List<MapSavePrefab> savedPrefabs) {
        var worldTransform = world.transform;
        foreach (var savedPrefab in savedPrefabs) {
            var info = prefabManager.GetInfoById(savedPrefab.id);
            if (info == null) {
                Debug.LogWarning("couldn't restore savedPrefab: " + savedPrefab);
                continue;
            }
            var prefab = ((PrefabInfo)info).managedPrefab;
			if (prefab == null) {
				Debug.LogWarning("prefab doesn't exist anymore: " + info.path);
				continue;
			}
            var prefabInstance = Instantiate(prefab, worldTransform, false);
            prefabInstance.name = savedPrefab.name;
            prefabInstance.SetActive(savedPrefab.active);
            prefabInstance.transform.position =
                        new Vector3(savedPrefab.x, savedPrefab.y, 0);
            if (prefabInstance.scalable) {
                if (savedPrefab.scale < 0.1f) {
                    savedPrefab.scale = 1f;
                }
                prefabInstance.transform.localScale =
                                  Vector3.one * savedPrefab.scale;
            }
            if (prefabInstance.varyColor && savedPrefab.color != null) {
                prefabInstance.Color = savedPrefab.color.Color();
            }
            if (prefabInstance.randomSprite) {
                prefabInstance.RandomSpriteIndex =
                                  savedPrefab.randomSpriteIndex;
            }
            if (prefabInstance is GameEntity) {
                var gameEntity = (GameEntity)prefabInstance;
                if (gameEntity.isContainer) {
                    gameEntity.startingContents = prefabManager.GetPrefabsByIds<Item>(
                        savedPrefab.startingContentsPrefabIds);
                    if (gameEntity is Character) {
                        var character = (Character)gameEntity;
                        character.startingEquipment = prefabManager.GetPrefabsByIds<Equipment>(
                            savedPrefab.startingEquipmentPrefabIds);
                        if (character is NonPlayerCharacter) {
                            var npc = (NonPlayerCharacter)character;
                            npc.trackDeath = savedPrefab.trackDeath;
                        }

                        var humanoidBody = character.GetComponent<HumanoidBody>();
                        if (humanoidBody != null &&
                            savedPrefab.savedHumanoidBody != null) {
                            savedPrefab.savedHumanoidBody.Restore(humanoidBody);
                        }
                    }
                    if (Application.isPlaying) {
                        gameEntity.RefreshSubItems();
                    }
                }
            }
            SetSortingOrder(prefabInstance, prefabsLoaded);
            prefabsLoaded++;

#if UNITY_EDITOR
            Undo.RegisterCreatedObjectUndo(prefabInstance.gameObject, "loaded map");
#endif
        }
    }

    protected void SetSortingOrder(ManagedPrefab prefab, int prefabsInScene) {
        var spriteRenderers = prefab.GetComponentsInChildren<SpriteRenderer>();
        foreach (var spriteRenderer in spriteRenderers) {
            spriteRenderer.sortingOrder += prefabsInScene * maxLayersInPrefab;
        }
    }

    private void ClearMap() {
        foreach (var tilemap in tilemaps) {
            var bounds = tilemap.cellBounds;
            for (int x = bounds.x; x <= bounds.xMax; x++) {
                for (int y = bounds.y; y <= bounds.yMax; y++) {
                    var position = new Vector3Int(x, y, 0);
                    tilemap.SetTile(position, null);
                }
            }
        }
        //tilemaps.ForEach((obj) => obj.ClearAllTiles());

        foreach (var gameEntity in world.RootLevelManagedPrefabs()) {
#if UNITY_EDITOR
            Undo.DestroyObjectImmediate(gameEntity.gameObject);
#else
            DestroyImmediate(gameEntity.gameObject);
#endif
        }
        prefabsLoaded = 0;
    }

}
