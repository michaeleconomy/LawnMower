﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerMapManager : MapManager {
    public static PlayerMapManager instance;

    private void Awake() {
        instance = this;
    }

    private readonly Dictionary<string, PlayerSavedMap> playerMaps =
        new Dictionary<string, PlayerSavedMap>();

    public Dictionary<string, ManagedPrefab> LoadPlayerSave(List<PlayerSavedMap> savedMaps, string _currentMap) {
        playerMaps.Clear();
        foreach (var savedMap in savedMaps) {
            playerMaps[savedMap.name] = savedMap;
        }
        return LoadPlayerMap(_currentMap);
    }

    public void NewGame() {
        playerMaps.Clear();
        LoadPlayerMap(mapData.startMap);
    }

    public void Travel(string mapName, string destinationName, int travelTime) {
        Overlay.Show("Traveling");
        if (mapName != currentMap) {
            SetPlayersParent(null);
            SaveCurrentPlayerMap();
            TurnManager.instance.RemoveNPCs();
            LoadPlayerMap(mapName);
            SetPlayersParent(world.transform);
            SetPlayersSortingOrder();
        }
        StartCoroutine(DelayedPlacePlayers(destinationName));
        if (travelTime > 0) {
            TimeManager.instance.AdvanceMinutes(travelTime);
            //TODO - give the players new actions?
            LogManager.Log("The party arrives in " + mapName + " after traveling for " + travelTime + " minutes");
            //TODO use a time formatter
        }
        TriggerManager.instance.CheckTriggers();
        Overlay.Hide();
    }

    private IEnumerator DelayedPlacePlayers(string destinationName) {
        yield return null;
        var destination = world.FindChildByName<ManagedPrefab>(destinationName);
        Vector3 position;
        if (destination != null) {
            position = destination.transform.position;
        }
        else {
            Debug.LogWarning("destination not found: " + destinationName);
            position = Vector3.zero;
        }
        PlayerPlacer.PlaceAround(position);
        var currentPlayer = TurnManager.instance.CurrentPlayer();
        if (currentPlayer != null) {
            yield return null; // wait for all of the locations to be updated.
            ActionController.instance.GetPlayerActions(currentPlayer);
        }
    }

    public bool HasGround(Vector3Int vector3Int) {
        return tilemaps[0].HasTile(vector3Int);
    }

    private void SetPlayersParent(Transform parent) {
        foreach (var player in TurnManager.instance.party) {
            player.transform.SetParent(parent);
        }
    }

    private void SetPlayersSortingOrder() {
        foreach (var player in TurnManager.instance.party) {
            SetSortingOrder(player, prefabsLoaded);
            prefabsLoaded++;
        }
    }

    public List<PlayerSavedMap> PlayerSavedMaps() {
        SaveCurrentPlayerMap();
        return playerMaps.Values.ToList();
    }

    //private below
    private Dictionary<string, ManagedPrefab> LoadPlayerMap(string mapName) {
        PlayerSavedMap playerMap;
        playerMaps.TryGetValue(mapName, out playerMap);

        LoadBaseMap(mapName, playerMap != null);
        if (playerMap == null) {
            Respawner.instance.DetectGraveyard();
            return null;
        }
        var managedPrefabs = new Dictionary<string, ManagedPrefab>();
        foreach (var savedPrefab in playerMap.savedPrefabs) {
            var prefabInfo = prefabManager.GetInfoById(savedPrefab.prefabId);
            var prefab = ((PrefabInfo)prefabInfo).managedPrefab;
            var managedPrefab = savedPrefab.Restore((ManagedPrefab)prefab);
            managedPrefabs[managedPrefab.Id()] = managedPrefab;
        }
        var count = 0;
        foreach (var savedPrefab in playerMap.savedPrefabs) {
            var managedPrefab = managedPrefabs[savedPrefab.id];
            savedPrefab.LoadReferences(managedPrefab, managedPrefabs);

            SetSortingOrder(managedPrefab, count);
            count++;
        }
        Respawner.instance.DetectGraveyard();
        return managedPrefabs;
    }

    private void SaveCurrentPlayerMap() {
        var savedPrefabs = new List<SavedPrefab>();
        var savablePrefabs =
            World.instance.GetComponentsInChildren<ManagedPrefab>(true);
        foreach (var savablePrefab in savablePrefabs) {
            savedPrefabs.Add(savablePrefab.Save());
        }

        playerMaps[currentMap] = new PlayerSavedMap {
            name = currentMap,
            savedPrefabs = savedPrefabs
        };
    }
}
