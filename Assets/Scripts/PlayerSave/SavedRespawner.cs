﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SavedRespawner {
    public string lastGraveyardArea;
    public string lastGraveyardName;

    public SavedRespawner() {

    }

    private SavedRespawner(Respawner respawner) {
        lastGraveyardArea = respawner.lastGraveyardArea;
        lastGraveyardName = respawner.lastGraveyardName;
    }


    public void Restore() {
        var respawner = Respawner.instance;
        respawner.lastGraveyardArea = lastGraveyardArea;
        respawner.lastGraveyardName = lastGraveyardName;
    }

    public static SavedRespawner Save() {
        return new SavedRespawner(Respawner.instance);
    }
}
