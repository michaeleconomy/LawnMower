﻿using System;

[Serializable]
public class SavedItem : SavedGameEntity {
    public int quantity = 1;

    public SavedItem() {

    }

    public SavedItem(Item i) : base(i) {
        quantity = i.Quantity;
    }

    public override ManagedPrefab Restore(ManagedPrefab prefab) {
        var item = (Item)base.Restore(prefab);
        item.Quantity = quantity;
        return item;
    }
}
