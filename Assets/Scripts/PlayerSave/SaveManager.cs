﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SaveManager : MonoBehaviour {
    public static string fileToLoad;

    private void Start() {
        var inGame = SceneManager.GetActiveScene().name.Contains("MainScene");
        if (!inGame) {
            return;
        }

        if (fileToLoad == null) {
            StartNewGame();
            return;
        }
        LoadSave(fileToLoad);
        fileToLoad = null;
    }

    public List<string> SaveFiles() {
        var files = Directory.GetFiles(SaveDirectory(), "??????????.save");

        //select only the filenames, and sort alphabetically
        var filesSorted = files.Select(Path.GetFileName).OrderBy(f => f);
        return new List<String>(filesSorted);
    }

    public void StartNewGame() {
        Overlay.Show("Initializing new game");
        QuestManager.instance.NewGame();
        DialogVariables.instance.Clear();
        PlayerMapManager.instance.NewGame();
        var player = World.instance.GetComponentInChildren<PlayerCharacter>();
        DialogVariables.instance.Set("met" + player.name);
        PlayerCharacter.primaryPlayer = player;
        TurnManager.instance.AddCharacter(player);
        TriggerManager.instance.readiedTriggers.Clear();
        TimeManager.instance.currentTime = 0;
        LogManager.Clear();
        StartCoroutine(StartTurnManagerLater(false));
    }

    private IEnumerator StartTurnManagerLater(bool fromSave) {
        yield return null;
        TurnManager.instance.StartMainLoop(fromSave);
        Overlay.Hide();
    }

    public string NewSave() {
        var fileName = GetNewSaveFileName();
        SaveToFile(fileName);
        return fileName;
    }

    public bool HasSave() {
        return LastSaveFileName() != null;
    }

    //public void LoadLastSave() {
    //    var lastSaveFile = LastSaveFileName();
    //    if (lastSaveFile == null) {
    //        throw new Exception("No save file!");
    //    }
    //    LoadSave(lastSaveFile);
    //}

    private void LoadSave(string fileName) {
        Overlay.Show("Loading Saved Game");
        Debug.Log("loading game from: " + SavePath(fileName));
        var save = FileJSONer.Deserialize<PlayerSave>(SavePath(fileName));

        //move the camera out so it doesn't get destroyed
        CameraController.Detach();

        var managedPrefabs =
            PlayerMapManager.instance.LoadPlayerSave(save.savedMaps,
                                                     save.currentMap);
        if (managedPrefabs == null) {
            throw new Exception("save for the current map was not found");
        }

        save.savedQuestManager.Restore();
        save.savedVariables.Restore();
        save.savedRespawner.Restore();

        TimeManager.instance.currentTime = save.currentTime;
        TriggerManager.instance.readiedTriggers.Clear();
        TriggerManager.instance.readiedTriggers.AddRange(save.triggers);

        PlayerCharacter.primaryPlayer =
            (PlayerCharacter)managedPrefabs[save.primaryPlayerId];

        save.savedTurnManager.Load(managedPrefabs);
        //restore camera (though this will get moved by actioncontroller
        CameraController.FocusOn(PlayerCharacter.primaryPlayer);
        LogManager.Clear();
        StartCoroutine(StartTurnManagerLater(false));
    }

    private string SaveDirectory() {
        var dir = Application.persistentDataPath + "/saves";
        if (!Directory.Exists(dir)) {
            Directory.CreateDirectory(dir);
        }
        return dir;
    }

    private string SavePath(string fileName) {
        return SaveDirectory() + "/" + fileName;
    }

    private void SaveToFile(string fileName) {
        var save = new PlayerSave() {
            savedMaps = PlayerMapManager.instance.PlayerSavedMaps(),
            currentMap = PlayerMapManager.instance.currentMap,
            primaryPlayerId = PlayerCharacter.primaryPlayer.Id(),
            savedTurnManager = new SavedTurnManager(TurnManager.instance),
            savedQuestManager = SavedQuestManager.Save(),
            savedVariables = SavedDialogVariables.Save(),
            savedRespawner = SavedRespawner.Save(),
            currentTime = TimeManager.instance.currentTime,
            triggers = TriggerManager.instance.readiedTriggers
        };

        var savePath = SavePath(fileName);
        FileJSONer.Serialize(save, savePath);
        LogManager.Log("game saved to: " + savePath);
    }

    private string GetNewSaveFileName() {
        var lastSaveFile = LastSaveFileName();
        if (lastSaveFile == null) {
            return "1000000000.save";
        }
        var numberPart = lastSaveFile.Substring(0, 10);
        var oldSaveNum = int.Parse(numberPart);
        var newSaveNum = oldSaveNum + 1;
        return newSaveNum.ToString() + ".save";
    }

    public string LastSaveFileName() {
        var saveFiles = SaveFiles();
        if (saveFiles.Count == 0) {
            return null;
        }
        return saveFiles[saveFiles.Count - 1];
    }
}
