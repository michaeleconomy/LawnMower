﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SavedPrefab {
    public string id;
    public string name;
    public int prefabId;
    public float x;
    public float y;
    public bool active;
    public float scale;
    public SavedColor color;
    public int randomSpriteIndex;

    public SavedPrefab() {

    }

    public SavedPrefab(ManagedPrefab prefab) {
        id = prefab.Id();
        prefabId = prefab.prefabId;
        var position = prefab.transform.position;
        x = position.x;
        y = position.y;
        name = prefab.name;
        active = prefab.active;
        scale = prefab.transform.localScale.x;
        if (prefab.varyColor) {
            color = new SavedColor(prefab.Color);
        }
        if (prefab.randomSprite) {
            randomSpriteIndex = prefab.RandomSpriteIndex;
        }
    }


    public virtual ManagedPrefab Restore(ManagedPrefab prefab) {
        var position = new Vector3(x, y, 0);
        var instance = UnityEngine.Object.Instantiate(prefab.gameObject,
                                                      position,
                                                      Quaternion.identity,
                                                      World.instance.transform);
        instance.name = name;

        var managedPrefab = instance.GetComponent<ManagedPrefab>();
        managedPrefab.id = id;
        managedPrefab.SetActive(active);
        if (managedPrefab.scalable) {
            managedPrefab.transform.localScale = Vector3.one * scale;
        }
        if (managedPrefab.varyColor && color != null) {
            managedPrefab.Color = color.Color();
        }
        if (managedPrefab.randomSprite) {
            managedPrefab.RandomSpriteIndex = randomSpriteIndex;
        }

        return managedPrefab;
    }

    public virtual void LoadReferences(ManagedPrefab managedPrefab,
            Dictionary<string, ManagedPrefab> managedPrefabs) {
    }
}
