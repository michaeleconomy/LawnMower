﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class SavedDialogVariables {
    public List<string> variables;

    public static SavedDialogVariables Save() {
        var variables = DialogVariables.instance.variables;
        return new SavedDialogVariables {
            variables = variables.ToList()
        };
    }

    public void Restore() {
        var variablesHash = new HashSet<string>(variables);
        DialogVariables.instance.variables = variablesHash;
    }
}
